from django.urls import include, path
from rest_framework.routers import DefaultRouter

from apps.accounts.urls import router as accounts_router
from apps.devices.urls import meter_status_router
from apps.devices.urls import router as meters_router
from apps.events.urls import meter_events_router
from apps.events.urls import router as events_router
from apps.locations.urls import router as locations_router
from apps.measurements.urls import meter_measurements_router
from apps.measurements.urls import router as measurements_router
from apps.organizations.urls import router as organizations_router
from apps.unifilar_diagram.urls import router as unifilar_diagram_router


class APIRouter(DefaultRouter):
    def extend(self, router):
        self.registry.extend(router.registry)
        return self


app_name = "api"

api_router = APIRouter()
api_router.extend(events_router)
api_router.extend(locations_router)
api_router.extend(measurements_router)
api_router.extend(organizations_router)
api_router.extend(meters_router)
api_router.extend(unifilar_diagram_router)
api_router.extend(accounts_router)

urlpatterns = [
    path("", include(api_router.urls)),
    path("", include(meter_measurements_router.urls)),
    path("", include(meter_events_router.urls)),
    path("", include(meter_status_router.urls)),
]

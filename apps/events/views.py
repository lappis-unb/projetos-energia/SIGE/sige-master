import logging

from rest_framework import exceptions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.common.pagination import BaseLimitOffsetPagination
from apps.common.serializers import DateRangeParamsValidator
from apps.devices.models import Meter
from apps.events.filters import EventFilterSet, EventSummaryFilterSet
from apps.events.models import (
    EnergyMeasurementTrigger,
    Event,
    InstantMeasurementTrigger,
)
from apps.events.serializers import (
    EnergyMeasurementTriggerSerializer,
    EventSerializer,
    EventsParamsSerializer,
    InstantMeasurementTriggerSerializer,
)
from apps.events.services import EventMetricsAnalyzer, EventReportService

logger = logging.getLogger("apps.events.views")


class EventLimitOffsetPagination(BaseLimitOffsetPagination):
    default_limit = 20
    max_limit = 100


class BaseEventViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Event.objects.all()
    filterset_class = EventFilterSet
    pagination_class = EventLimitOffsetPagination
    serializer_class = EventSerializer
    query_params_class = EventsParamsSerializer

    parent_lookup = "meter_pk"

    def get_queryset(self):
        queryset = (
            super()
            .get_queryset()
            .select_related(
                "meter",
                "trigger",
                "trigger__meterstatustrigger",
                "trigger__instantmeasurementtrigger",
                "trigger__energymeasurementtrigger",
            )
        )

        if self.parent_lookup not in self.kwargs:
            return queryset

        try:
            meter_id = int(self.kwargs[self.parent_lookup])
            filtered_queryset = queryset.filter(meter_id=meter_id)
            self._validate_set_lookup(filtered_queryset, meter_id)
            return filtered_queryset
        except ValueError:
            raise exceptions.NotFound("Invalid URL: meter ID must be a positive number")

    def _validate_set_lookup(self, queryset, meter_id):
        try:
            self._meter = queryset.first().meter
        except AttributeError:
            if not Meter.objects.filter(id=meter_id).exists():
                raise exceptions.NotFound("Meter not found")
            raise exceptions.NotFound("No status found for this meter.")

    def get_meter(self):
        return getattr(self, "_meter", None)

    def get_filterset_class(self):
        if self.action == "summary":
            return EventSummaryFilterSet
        return self.filterset_class

    def get_query_params_class(self):
        if self.action == "summary":
            return DateRangeParamsValidator
        return self.query_params_class

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["meter_pk"] = self.kwargs.get("meter_pk")
        return context

    def filter_queryset(self, queryset):
        self.validated_params = self._validate_params(self.request.query_params)
        filterset_class = self.get_filterset_class()
        filterset = filterset_class(data=self.validated_params, queryset=queryset)
        return filterset.qs

    def _validate_params(self, query_params, raise_exception=True):
        validade_params_class = self.get_query_params_class()
        params_serializer = validade_params_class(data=query_params)
        params_serializer.is_valid(raise_exception=raise_exception)
        return params_serializer.validated_data


class MeterEventViewSet(BaseEventViewSet):
    @action(methods=["get"], detail=False)
    def summary(self, request, meter_pk=None):
        events_qs = self.filter_queryset(self.get_queryset())
        meter = self.get_meter()

        analytics = EventMetricsAnalyzer(events_qs)
        report_service = EventReportService(analytics, self.validated_params)

        response_data = report_service.get_meter_report(meter)
        return Response(response_data, status=status.HTTP_200_OK)


class GlobalEventViewSet(BaseEventViewSet):
    def list(self, request, *args, **kwargs):
        events_qs = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(events_qs)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return Response("Pagination error", status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(detail=False, methods=["get"])
    def summary(self, request):
        events_qs = self.filter_queryset(self.get_queryset())

        analytics = EventMetricsAnalyzer(events_qs)
        report_service = EventReportService(analytics, self.validated_params)

        response_data = report_service.get_general_report()
        return Response(response_data, status=status.HTTP_200_OK)


class InstantMeasurementTriggerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = InstantMeasurementTrigger.objects.all()
    serializer_class = InstantMeasurementTriggerSerializer


class EnergyMeasurementTriggerViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = EnergyMeasurementTrigger.objects.all()
    serializer_class = EnergyMeasurementTriggerSerializer

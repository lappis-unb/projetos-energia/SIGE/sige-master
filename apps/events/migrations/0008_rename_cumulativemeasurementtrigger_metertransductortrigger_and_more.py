from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("events", "0007_alter_instantmeasurementtrigger_field_name"),
        ("devices", "0002_rename_transductor_to_meter_tables"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="CumulativeMeasurementTrigger",
            new_name="EnergyMeasurementTrigger",
        ),
        migrations.AlterModelOptions(
            name="energymeasurementtrigger",
            options={
                "verbose_name": "Energy Measurement Trigger",
                "verbose_name_plural": "Energy Measurement Triggers",
            },
        ),
        migrations.RenameField(
            model_name="event",
            old_name="transductor",
            new_name="meter",
        ),
        migrations.RenameModel(
            old_name="TransductorStatusTrigger",
            new_name="MeterStatusTrigger",
        ),
        migrations.AlterField(
            model_name="event",
            name="meter",
            field=models.ForeignKey(on_delete=models.deletion.CASCADE, related_name="events", to="devices.meter"),
        ),
        migrations.RenameField(
            model_name="meterstatustrigger",
            old_name="transductors",
            new_name="meters",
        ),
        migrations.AlterModelOptions(
            name="meterstatustrigger",
            options={
                "verbose_name": "Meter Status Trigger",
                "verbose_name_plural": "Meter Status Triggers",
            },
        ),
    ]

from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter

from apps.devices.urls import router as meters_base_router
from apps.events.views import (
    EnergyMeasurementTriggerViewSet,
    GlobalEventViewSet,
    InstantMeasurementTriggerViewSet,
    MeterEventViewSet,
)

app_name = "events"

router = DefaultRouter()
router.register(r"instant-triggers", InstantMeasurementTriggerViewSet, basename="instant-triggers")
router.register(r"energy-triggers", EnergyMeasurementTriggerViewSet, basename="energy-triggers")
router.register(r"meters/events", GlobalEventViewSet, basename="meters-events")


meter_events_router = NestedDefaultRouter(meters_base_router, r"meters", lookup="meter")
meter_events_router.register(r"events", MeterEventViewSet, basename="meters-events")


urlpatterns = [
    path("", include(router.urls)),
    path("", include(meter_events_router.urls)),
]

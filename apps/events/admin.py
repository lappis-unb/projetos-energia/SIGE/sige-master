from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.utils.translation import gettext_lazy as _

from apps.devices.models import Meter
from apps.events.models import (
    CategoryTrigger,
    EnergyMeasurementTrigger,
    Event,
    InstantMeasurementTrigger,
    MeterStatusTrigger,
    SeverityTrigger,
    Trigger,
)


@admin.register(Trigger)
class TriggerAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "severity",
        "category",
        "is_active",
        "created_at",
        "updated_at",
    )
    list_filter = ("category", "is_active", "severity")
    search_fields = ("name", "notification_message")
    readonly_fields = ("created_at", "updated_at")


@admin.register(InstantMeasurementTrigger)
class InstantMeasurementTriggerAdmin(TriggerAdmin):
    list_display = TriggerAdmin.list_display + (
        "id",
        "field_name",
        "lower_threshold",
        "upper_threshold",
        "notification_message",
    )
    list_filter = TriggerAdmin.list_filter + ("field_name",)
    search_fields = TriggerAdmin.search_fields + ("field_name",)

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "severity",
                    "category",
                    "field_name",
                    "lower_threshold",
                    "upper_threshold",
                    "notification_message",
                    "is_active",
                )
            },
        ),
    )


@admin.register(EnergyMeasurementTrigger)
class EnergyMeasurementTriggerAdmin(TriggerAdmin):
    list_display = TriggerAdmin.list_display + (
        "field_name",
        "dynamic_metric",
        "upper_threshold_percent",
        "lower_threshold_percent",
        "period_days",
        "notification_message",
    )
    list_filter = TriggerAdmin.list_filter + ("dynamic_metric", "field_name")
    search_fields = TriggerAdmin.search_fields + ("field_name",)

    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "severity",
                    "category",
                    "field_name",
                    "dynamic_metric",
                    "upper_threshold_percent",
                    "lower_threshold_percent",
                    "period_days",
                    "notification_message",
                    "is_active",
                ),
            },
        ),
    )


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = (
        "trigger__pk",
        "meter",
        "display_name",
        "trigger__field_name",
        "trigger__target_status",
        "display_severity",
        "display_category",
        "created_at",
        "ended_at",
        "is_active",
    )
    readonly_fields = (
        "created_at",
        "ended_at",
        "display_name",
        "display_severity",
        "display_category",
    )
    list_filter = ("is_active", "meter")
    search_fields = ("name", "meter__name")
    date_hierarchy = "created_at"
    actions = ["close_events"]

    def display_name(self, obj):
        return obj.name

    display_name.short_description = _("Name")

    def display_severity(self, obj):
        severity = obj.severity
        return SeverityTrigger(severity).label

    display_severity.short_description = _("Severity")

    def display_category(self, obj):
        category = obj.category
        return CategoryTrigger(category).label

    display_category.short_description = _("Category")

    def close_events(self, request, queryset):
        for event in queryset:
            if event.is_active:
                event.close_event()
        self.message_user(request, _("Selected events have been closed."))

    close_events.short_description = _("Close selected events")

    def trigger__pk(self, obj):
        return obj.trigger.pk

    trigger__pk.short_description = _("Trigger ID")

    def trigger__field_name(self, obj):
        if obj.trigger.instantmeasurementtrigger:
            return obj.trigger.instantmeasurementtrigger.field_name
        elif obj.trigger.energymeasurementtrigger:
            return obj.trigger.energymeasurementtrigger.field_name
        return "N/A"

    trigger__field_name.short_description = _("Field Name")

    def trigger__target_status(self, obj):
        if obj.trigger.meterstatustrigger:
            return obj.trigger.meterstatustrigger.get_target_status_display()
        return "N/A"

    trigger__target_status.short_description = _("Target Status")


# ______________________________________________________________________________


class MeterStatusTriggerForm(forms.ModelForm):
    meters = forms.ModelMultipleChoiceField(
        queryset=Meter.objects.all(),
        required=False,
        blank=True,
        widget=FilteredSelectMultiple(_("Meters"), is_stacked=False),
    )

    class Meta:
        model = MeterStatusTrigger
        fields = "__all__"


@admin.register(MeterStatusTrigger)
class MeterStatusTriggerAdmin(admin.ModelAdmin):
    form = MeterStatusTriggerForm
    list_display = (
        "name",
        "target_status",
        "severity",
        "category",
        "threshold_time",
        "is_active",
        "created_at",
    )
    list_filter = ("category", "is_active", "severity", "category")
    search_fields = ("name",)
    readonly_fields = ("created_at", "updated_at")
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "severity",
                    "category",
                    "target_status",
                    "meters",
                    "threshold_time",
                    "is_active",
                    "notification_message",
                )
            },
        ),
    )

    def display_meters(self, obj):
        return ", ".join([meter.ip_address for meter in obj.meters.all()])

    display_meters.short_description = _("Meters")

    def target_state(self, obj):
        return obj.get_target_status_display()

    target_state.short_description = _("Target State")

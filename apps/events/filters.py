from django_filters import rest_framework as filters

from apps.events.models import Event


class EventSummaryFilterSet(filters.FilterSet):
    start_date = filters.DateTimeFilter(field_name="created_at", lookup_expr="gte")
    end_date = filters.DateTimeFilter(field_name="created_at", lookup_expr="lte")

    class Meta:
        model = Event
        fields = ["start_date", "end_date"]


class EventFilterSet(filters.FilterSet):
    start_date = filters.DateTimeFilter(field_name="created_at", lookup_expr="gte")
    end_date = filters.DateTimeFilter(field_name="created_at", lookup_expr="lte")
    severity = filters.NumberFilter(field_name="trigger__severity")
    category = filters.NumberFilter(field_name="trigger__category")
    is_active = filters.BooleanFilter(field_name="is_active")

    class Meta:
        model = Event
        fields = ["start_date", "end_date", "severity", "category", "is_active"]

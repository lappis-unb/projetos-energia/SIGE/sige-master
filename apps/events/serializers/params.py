import logging

from rest_framework import serializers
from rest_framework.serializers import ValidationError

from apps.common.serializers import DateRangeParamsValidator
from apps.events.models import CategoryTrigger, SeverityTrigger

logger = logging.getLogger("apps.events.serializers")


class EventsParamsSerializer(DateRangeParamsValidator):
    severity = serializers.CharField(required=False)
    category = serializers.CharField(required=False)
    is_active = serializers.BooleanField(required=False, allow_null=True)

    class Meta:
        model = SeverityTrigger
        params = ["start_date", "end_date", "last", "severity", "category", "is_active"]

    def validate_category(self, value):
        if value.isdigit() and int(value) in CategoryTrigger.values:
            return int(value)

        if value.upper() in CategoryTrigger.names:
            return CategoryTrigger[value.upper()].value

        for category in CategoryTrigger:
            if category.label.upper() == value.upper():
                return category.value

        category_choices = [f"{c.value} ({c.label.lower()})" for c in CategoryTrigger]
        raise ValidationError(f"Invalid category: {value}. Use: {', '.join(category_choices)}")

    def validate_severity(self, value):
        if value.isdigit() and int(value) in SeverityTrigger.values:
            return int(value)

        if value.upper() in SeverityTrigger.names:
            return SeverityTrigger[value.upper()].value

        for severity in SeverityTrigger:
            if severity.label.upper() == value.upper():
                return severity.value

        severity_choices = [f"{s.value} ({s.label.lower()})" for s in SeverityTrigger]
        raise ValidationError(f"Invalid severity: {value}. Use: {', '.join(severity_choices)}")

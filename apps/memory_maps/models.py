import logging

from django.db import models

from apps.devices.models import MeterModel
from apps.memory_maps.modbus.helpers import type_modbus
from apps.memory_maps.modbus.settings import CSV_SCHEMA

logger = logging.getLogger("apps")


class MemoryMap(models.Model):
    model = models.OneToOneField(MeterModel, on_delete=models.CASCADE, related_name="memory_map")
    instant_measurements = models.JSONField()
    energy_measurements = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.model.name}"

    class Meta:
        verbose_name_plural = "Memory Maps"

    def get_memory_map_by_type(self, data_type: str):
        # TODO - move string literals to constants in settings or utils
        if data_type in {"instant", "realtime", "minutely"}:
            return self.instant_measurements
        elif data_type in {"energy", "aggregate", "quarterly"}:
            return self.energy_measurements
        return {}

    @classmethod
    def create_from_csv(cls, meter_model, csv_data, max_block):
        instance = cls(model=meter_model, instant_measurements={}, energy_measurements={})

        instance.instant_measurements = instance._process_csv_data("minutely", csv_data, max_block)
        instance.energy_measurements = instance._process_csv_data("quarterly", csv_data, max_block)
        return instance.save()

    def update_from_csv(self, csv_data: list[dict], max_block: int):
        self.instant_measurements = self._process_csv_data("minutely", csv_data, max_block)
        self.energy_measurements = self._process_csv_data("quarterly", csv_data, max_block)
        return self.save()

    def _process_csv_data(self, data_group, csv_data, max_block):
        datagroup_registers = self._get_valid_registers_by_group(csv_data, data_group)

        if not datagroup_registers:
            return {}

        return self._build_sequential_blocks(datagroup_registers, max_block)

    def _get_valid_registers_by_group(self, csv_data: list[dict], data_group: str) -> list[dict]:
        required_headers = CSV_SCHEMA.keys()

        try:
            datagroup_registers = []
            for line in csv_data:
                if line["group"] != data_group:
                    continue

                filtered_line = {column: line[column] for column in line if column in required_headers}
                filtered_line.update(
                    {
                        "address": int(line["address"]),
                        "size": int(line["size"]),
                        "type": type_modbus(line["type"]),
                    }
                )
                datagroup_registers.append(filtered_line)
            return datagroup_registers

        except (TypeError, ValueError, KeyError) as e:
            raise ValueError(f"Error occurred while processing data: {e}") from e

    def _build_sequential_blocks(self, registers: list[dict], len_max_block: int) -> list[dict]:
        """
        Group consecutive registers with type and until the `len_max_block` number of registers
        is reached or the next register has a different address or type than the previous one.
        It then adds the block of consecutive registers to a list of sequential blocks.
        """

        sequential_blocks = []
        current_block = {
            "start_address": registers[0]["address"],
            "size": registers[0]["size"],
            "type": registers[0]["type"],
            "byteorder": registers[0]["byteorder"],
            "function": registers[0]["function"],
            "attributes": [registers[0]["attribute"]],
        }

        register_counter = 1
        for i in range(1, len(registers)):
            current_line = registers[i]

            check_conditions = all(
                [
                    current_line["address"] == current_block["start_address"] + current_block["size"],
                    current_line["type"] == current_block["type"],
                    register_counter < len_max_block,
                ]
            )

            if check_conditions:
                current_block["size"] += current_line["size"]
                register_counter += 1
                current_block["attributes"].append(current_line["attribute"])

            else:
                sequential_blocks.append(current_block)
                current_block = {
                    "start_address": current_line["address"],
                    "size": current_line["size"],
                    "type": current_line["type"],
                    "byteorder": current_line["byteorder"],
                    "function": current_line["function"],
                    "attributes": [current_line["attribute"]],
                }
                register_counter = 1

        sequential_blocks.append(current_block)
        return sequential_blocks

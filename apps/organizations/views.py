from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.organizations.models import Entity, Organization
from apps.organizations.serializers import (
    EntityBaseSerializer,
    EntityCreateSerializer,
    EntityDetailSerializer,
    EntityTreeListSerializer,
    OrganizationCreateSerializer,
)


class EntityViewSet(viewsets.ModelViewSet):
    queryset = Entity.objects.all()
    serializer_class = EntityBaseSerializer

    def get_serializer_class(self, *args, **kwargs):
        if self.action in ["create", "update", "partial_update"]:
            return EntityCreateSerializer
        if self.action == "retrieve":
            return EntityDetailSerializer
        if self.action == "tree":
            return EntityTreeListSerializer
        return super().get_serializer_class()

    @action(detail=True, methods=["get"])
    def descendants(self, request, pk=None):
        root_entity = self.get_object()
        serializer = EntityTreeListSerializer(root_entity)
        return Response(serializer.data)

    @action(detail=False, methods=["get"])
    def tree(self, request):
        queryset = self.get_queryset().filter(parent=None)
        serializer = self.get_serializer_class()(queryset, many=True)
        return Response(serializer.data)


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = Organization.objects.all()
    serializer_class = OrganizationCreateSerializer

from django.db import models


class EntityQuerySet(models.QuerySet):
    def get_descendant_ids(self, root_id, include_self=True):
        if not root_id:
            return []

        child_pairs = list(self.exclude(parent_id=None).values_list("parent_id", "id"))
        descendant_ids = {root_id} if include_self else set()
        current_level = {root_id}

        while current_level:
            next_level = {child_id for parent_id, child_id in child_pairs if parent_id in current_level}
            if not next_level:
                break

            descendant_ids.update(next_level)
            current_level = next_level

        return list(descendant_ids)

    def get_descendants(self, root_id, include_self=True):
        descendant_ids = self.get_descendant_ids(root_id, include_self=include_self)
        return self.filter(id__in=descendant_ids)


class EntityManager(models.Manager):
    def get_queryset(self):
        return EntityQuerySet(self.model, using=self._db)

    def get_descendant_ids(self, root_id, include_self=True):
        queryset = self.get_queryset()
        return queryset.get_descendant_ids(root_id, include_self=include_self)

    def get_descendants(self, root_id, include_self=True):
        queryset = self.get_queryset()
        return queryset.get_descendants(root_id, include_self=include_self)

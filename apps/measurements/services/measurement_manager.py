import logging

from datetime import timedelta

from django.db import transaction

from apps.measurements.models import EnergyMeasurement

MINUTES_INTERVAL = 15
logger = logging.getLogger("apps")


class EnergyMeasurementManager:
    """
    A class to manage the creation and handling of Energy measurements based on provided data.

    This class provides functionality to calculate missing measurement intervals, distribute
    measurement deltas across these intervals, and create new EnergyMeasurement instances
    to fill in gaps in measurement data. The creation of new measurements based on the validated
    input data and fields specified.

    Methods:
        handle_Energy_measurements(validated_data, last_measurement, fields):
            The main method intended for external use. It processes the validated data to
            manage and create new Energy measurement instances.

        _calculate_intervals_missing(last_measurement, validated_data):
            Calculate the number of time intervals missing between the last recorded measurement
            and the new data provided.

        _calculate_delta_fields(last_measurement, validated_data, fields):
            Calculate the delta for specified fields between the last measurement and new validated data.

        _distribute_interval_measurements(delta_data, intervals):
            Distribute the calculated deltas evenly over the number of missing intervals.

        _create_missing_measurements(last_measurement, data_chunk, intervals_missed):
            Create and return a list of new EnergyMeasurement instances to fill in the missing intervals.

    Usage:
        - To create new Energy measurements, instantiate this class and call the
          `handle_Energy_measurements` method with appropriate data.

    Example:
        >>> validated_data = {'collection_date': datetime.datetime(2021, 5, 15), 'active_consumption': 19, ....}
        >>> last_measurement = EnergyMeasurement.objects.get(id=1)
        >>> fields = ['active_consumption', 'active_generated', ....]
        >>> measurements = MeasurementCreator.handle_Energy_measurements(validated_data, last_measurement, fields)
    """

    @classmethod
    def handle_measurements(cls, validated_data, last_measurement, fields):
        """This is the only method that should be called from outside the class"""

        intervals_missed = cls._calculate_intervals_missing(last_measurement, validated_data)

        delta_data = cls._calculate_delta_fields(last_measurement, validated_data, fields)
        delta_validated_data = validated_data.copy()
        delta_validated_data.update(delta_data)

        if intervals_missed <= 1:
            return EnergyMeasurement.objects.create(**delta_validated_data)

        data_chunk = cls._distribute_interval_measurements(delta_validated_data, intervals_missed, fields)
        return cls._create_missing_instances(last_measurement, data_chunk, intervals_missed)

    @classmethod
    def _calculate_delta_fields(cls, last_measurement, validated_data, fields):
        delta_data = {}
        for field in fields:
            if field in validated_data:
                last_value = getattr(last_measurement, field, 0)
                new_value = validated_data[field]
                if new_value < last_value:
                    logger.warning(f"Invalid measurement value for field '{field}': {new_value}")
                    raise ValueError(f"Invalid measurement value for field '{field}': {new_value}")
                if new_value is None:
                    logger.warning(f"Invalid new measurement value for field '{field}': {new_value}")
                    new_value = 0

                if last_value is None:
                    logger.warning(f"Invalid last measurement value for field '{field}': {last_value}")
                    last_value = 0

                delta = new_value - last_value
                delta_data[field] = delta
                logger.debug(f"Calculated delta for field '{field}': {delta}")
        return delta_data

    @classmethod
    def _calculate_intervals_missing(cls, last_measurement, validated_data):
        delta_time = validated_data["collection_date"] - last_measurement.updated
        intervals_missing = delta_time / timedelta(minutes=MINUTES_INTERVAL)
        return max(int(intervals_missing), 0)

    @classmethod
    def _distribute_interval_measurements(cls, delta_data, intervals_missed, energy_fields):
        return {
            field: round(value / intervals_missed, 2)  # Delta value divided by missing intervals
            for field, value in delta_data.items()
            if field in energy_fields
        }

    @classmethod
    def _create_missing_instances(cls, last_measurement, data_chunk, intervals_missed):
        start_time = last_measurement.updated
        meter = last_measurement.meter

        if start_time is None or meter is None:
            logger.warning("Invalid last measurement data: 'updated' and 'meter' fields are required.")
            raise ValueError("last_measurement must have 'updated' and 'meter' fields")

        with transaction.atomic():
            instances = []
            for pos in range(intervals_missed):
                timestamp = start_time + timedelta(minutes=15 * pos)
                instance = EnergyMeasurement(
                    meter=meter,
                    collection_date=timestamp,
                    is_calculated=True,
                    **data_chunk,
                )
                instances.append(instance)

            with transaction.atomic():
                EnergyMeasurement.objects.bulk_create(instances)

            return instances

import logging

from concurrent.futures import ThreadPoolExecutor, as_completed

import numpy as np
import pandas as pd

from apps.utils.helpers import log_service

logger = logging.getLogger("apps.measurements.services.downsampler")


class LTTBDownSampler:
    """A service class for downsampling time series data using the LTTB algorithm,
    supporting combined and reference-based indexing."""

    def __init__(self, n_out, enable_parallel=False):
        if n_out <= 2:
            raise ValueError("n_out should be greater than 2.")

        self.n_out = n_out
        self.enable_parallel = enable_parallel

    @log_service()
    def apply_lttb(self, data, fields, dt_field, ref_field=None):
        if self.n_out > data.shape[0]:
            return data

        try:
            self._validate_inputs(data, dt_field, fields, ref_field)
            df_prepared = self._prepare_dataframe(data, fields, dt_field)
            indices = self._downsample_dataframe(df_prepared, fields, ref_field)
            return data.loc[indices]
        except ValueError as e:
            logger.error(f"Error in apply_lttb: {str(e)}")
            raise

    def _prepare_dataframe(self, data, fields, dt_field):
        selected_columns = [dt_field] + fields
        df = data[selected_columns]
        df = df.rename(columns={dt_field: "x"})
        df["x"] = pd.to_datetime(df["x"]).apply(lambda x: x.timestamp())
        return df

    def _downsample_dataframe(self, df, fields, ref_field):
        if ref_field:
            downsample_df = df[["x", ref_field]].rename(columns={ref_field: "y"})
            return self._downsample_signal(downsample_df)

        if self.enable_parallel and len(fields) > 1:
            return self._parallel_downsample_multi_signals(df, fields)

        return self._downsample_multi_signals(df, fields)

    def _parallel_downsample_multi_signals(self, df, fields):
        indices = []
        num_workers = min(len(fields), 8)
        with ThreadPoolExecutor(max_workers=num_workers) as executor:
            futures = []
            for field in fields:
                df_signal = df[["x", field]].rename(columns={field: "y"})
                futures.append(executor.submit(self._downsample_signal, df_signal))

            for future in as_completed(futures):
                indices.extend(future.result())
        return np.unique(indices)

    def _downsample_multi_signals(self, df, fields):
        indices = []
        for field in fields:
            df_signal = df[["x", field]].rename(columns={field: "y"})
            indices_column = self._downsample_signal(df_signal)
            indices.extend(indices_column)
        return np.unique(indices)

    def _downsample_signal(self, df):
        if df["y"].dtype != float:
            df["y"] = df["y"].astype(float, errors="ignore")

        data = df[["x", "y"]].to_numpy()
        return self._lttb_core(data)

    def _lttb_core(self, df):
        n_bins = self.n_out - 2
        data_bins = np.array_split(df[1:-1], n_bins)
        indices = np.zeros(self.n_out, dtype=int)
        indices[0], indices[-1] = 0, len(df) - 1
        start_indices = self._calculate_start_indices(data_bins)

        for i in range(n_bins):
            a = df[indices[i]]
            bs = data_bins[i]
            next_bin = data_bins[i + 1] if i < n_bins - 1 else df[-1:]
            c = next_bin.mean(axis=0)
            areas = self._areas_of_triangles(a, bs, c)
            max_index = np.argmax(areas)
            indices[i + 1] = start_indices[i] + max_index
        return indices

    def _areas_of_triangles(self, a, bs, c):
        a_to_c = c - a
        return 0.5 * np.abs(a_to_c[0] * (bs[:, 1] - a[1]) - a_to_c[1] * (bs[:, 0] - a[0]))

    def _calculate_start_indices(self, data_bins):
        start_indices = [1]
        for i in range(1, len(data_bins)):
            start_indices.append(start_indices[-1] + len(data_bins[i - 1]))
        return start_indices

    def _validate_inputs(self, data, dt_field, fields, ref_field):
        if not isinstance(data, pd.DataFrame):
            raise ValueError("Input data should be a pandas DataFrame.")

        if dt_field not in data.columns:
            raise ValueError(f"Column '{dt_field}' not found in the DataFrame.")

        if ref_field and ref_field not in data.columns:
            raise ValueError(f"Column '{ref_field}' selected as reference not found in the DataFrame.")

        if len(data.columns) < 2:
            raise ValueError("Dataframe should have at least 2 columns.")

        if not fields:
            raise ValueError("Fields for downsampling should be selected.")
        else:
            if not all(field in data.columns for field in fields):
                raise ValueError("Fields not found in the DataFrame.")

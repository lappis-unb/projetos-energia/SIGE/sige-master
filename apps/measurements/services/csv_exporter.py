import csv

from datetime import datetime
from decimal import Decimal
from io import StringIO

from django.utils import timezone


class MeasurementCSVExporter:
    def __init__(self, queryset, fields=None):
        self.queryset = queryset
        self.fields = fields
        self.tz = timezone.get_current_timezone()

    def export_csv(self):
        if not self.fields:
            self.fields = [key for key in self.queryset[0].keys() if key != "id"]

        return self.generate_csv()

    def generate_csv(self):
        csv_file = StringIO()
        csv_writer = csv.writer(csv_file)
        csv_writer.writerow(self.fields)

        for instance in self.queryset:
            row = []
            for field in self.fields:
                value = instance.get(field)
                row.append(self._format_value(value))
            csv_writer.writerow(row)
        csv_file.seek(0)

        return csv_file.getvalue()

    def _format_value(self, value):
        match value:
            case datetime():
                return value.astimezone(self.tz).isoformat()
            case Decimal():
                return str(value)
            case str():
                return value.encode("utf-8")
            case int() | float():
                return value
            case _:
                return str(value)

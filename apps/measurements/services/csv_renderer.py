from rest_framework.renderers import BaseRenderer

from apps.measurements.services.csv_exporter import MeasurementCSVExporter


class MeasurementCSVRenderer(BaseRenderer):
    media_type = "text/csv"
    format = "csv"
    charset = "utf-8"

    def render(self, data, media_type=None, renderer_context=None):
        if data is None:
            return ""

        if isinstance(data, str):
            return data

        csv_exporter = MeasurementCSVExporter(queryset=data)
        return csv_exporter.export_csv()

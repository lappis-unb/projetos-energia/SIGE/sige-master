
from django.db.models import Q
from django.db.models.functions import ExtractHour
from django_filters import rest_framework as filters

from .models import EnergyMeasurement, InstantMeasurement


class BaseMeasurementFilter(filters.FilterSet):
    meter = filters.NumberFilter(field_name="meter")
    start_date = filters.DateTimeFilter(field_name="collection_date", lookup_expr="gte")
    end_date = filters.DateTimeFilter(field_name="collection_date", lookup_expr="lte")
    only_day = filters.BooleanFilter(method="filter_only_day")
    fields = filters.BaseInFilter(method="filter_fields")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        allowed_fields = set(self.Meta.fields)
        for field in list(self.base_filters.keys()):
            if field not in allowed_fields:
                del self.base_filters[field]

    class Meta:
        model = None
        fields = ["meter", "start_date", "end_date", "only_day", "fields"]

    def filter_only_day(self, queryset, name, value):
        if value:
            queryset = queryset.annotate(hour=ExtractHour("collection_date"))
            queryset = queryset.filter(Q(hour__gte=6) & Q(hour__lt=19))
        return queryset

    def filter_fields(self, queryset, name, value):
        fields = ["id", "meter", "collection_date"] + value
        return queryset.values(*fields)


class InstantMeasurementFilter(BaseMeasurementFilter):
    class Meta:
        model = InstantMeasurement
        fields = ["meter", "start_date", "end_date", "fields"]


class EnergyMeasurementFilter(BaseMeasurementFilter):
    class Meta:
        model = EnergyMeasurement
        fields = ["meter", "start_date", "end_date", "fields", "only_day"]


class DailyProfileFilter(BaseMeasurementFilter):
    peak_hours = filters.BooleanFilter(method="filter_peak_hours")
    off_peak_hours = filters.BooleanFilter(method="filter_off_peak_hours")

    peak_start_hour = 18
    peak_end_hour = 21

    class Meta:
        model = EnergyMeasurement
        fields = ["meter", "start_date", "end_date", "peak_hours", "off_peak_hours"]

    def filter_off_peak_hours(self, queryset, name, value):
        queryset = self._annotate_hour(queryset)
        return queryset.exclude(Q(hour__gte=self.peak_start_hour) & Q(hour__lt=self.peak_end_hour))

    def filter_peak_hours(self, queryset, name, value):
        queryset = self._annotate_hour(queryset)
        return queryset.filter(Q(hour__gte=self.peak_start_hour) & Q(hour__lt=self.peak_end_hour))

    def _annotate_hour(self, queryset):
        return queryset.annotate(hour=ExtractHour("collection_date"))

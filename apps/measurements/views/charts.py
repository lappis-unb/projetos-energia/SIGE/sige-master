import logging

import pandas as pd
from django.conf import settings
from drf_spectacular.utils import extend_schema
from rest_framework import exceptions, status
from rest_framework.exceptions import ValidationError
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from apps.measurements.filters import (
    DailyProfileFilter,
    EnergyMeasurementFilter,
    InstantMeasurementFilter,
)
from apps.measurements.models import EnergyMeasurement, InstantMeasurement
from apps.measurements.serializers import (
    ChartMeasurementSerializer,
    DailyProfileParamsValidator,
    DailyProfileSerializer,
    EnergyChartParamsValidator,
    InstantChartParamsValidator,
)
from apps.measurements.services.downsampler import LTTBDownSampler

logger = logging.getLogger("apps.measurements.views")


@extend_schema(parameters=[InstantChartParamsValidator])
class InstantChartViewSet(ListModelMixin, GenericViewSet):
    queryset = InstantMeasurement.objects.all()
    filterset_class = InstantMeasurementFilter
    serializer_class = ChartMeasurementSerializer
    query_params_class = InstantChartParamsValidator

    def get_queryset(self):
        queryset = super().get_queryset()
        meter_id = self.kwargs.get("meter_pk")

        if meter_id:
            return queryset.select_related("meter").filter(meter_id=meter_id)
        return queryset

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["meter_pk"] = self.kwargs.get("meter_pk")
        context["fields"] = self.validated_params.get("fields")
        return context

    def filter_queryset(self, queryset):
        self.validated_params = self._validate_params(self.request.query_params)
        filterset = self.filterset_class(data=self.validated_params, queryset=queryset)
        return filterset.qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if not queryset.exists():
            return Response("No data found", status=status.HTTP_204_NO_CONTENT)

        data = pd.DataFrame(queryset)
        if self.validated_params.get("lttb"):
            threshold = self.validated_params.get("threshold") or settings.LTTB_THRESHOLD_POINTS
            fields = self.validated_params.get("fields", [])
            data = self._apply_lttb(data, threshold, fields)

        serializer = self.get_serializer(data)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def _apply_lttb(self, df, threshold, fields):
        try:
            downsampler = LTTBDownSampler(threshold, enable_parallel=True)
            return downsampler.apply_lttb(df, fields=fields, dt_field="collection_date")
        except Exception as e:
            logger.error(f"Error in apply_lttb: {e}")
            raise ValidationError({"error": str(e)})

    def _validate_params(self, query_params, raise_exception=True):
        params_serializer = self.query_params_class(data=query_params)
        params_serializer.is_valid(raise_exception=raise_exception)
        return params_serializer.validated_data


@extend_schema(parameters=[EnergyChartParamsValidator])
class EnergyChartViewSet(ListModelMixin, GenericViewSet):
    queryset = EnergyMeasurement.objects.all()
    filterset_class = EnergyMeasurementFilter
    serializer_class = ChartMeasurementSerializer
    query_params_class = EnergyChartParamsValidator

    def get_queryset(self):
        queryset = super().get_queryset()
        meter_id = self.kwargs.get("meter_pk")

        try:
            meter_id = int(meter_id)
            if meter_id <= 0:
                raise ValueError
        except ValueError:
            raise exceptions.NotFound("Invalid URL: meter ID must be a positive number")

        return queryset.select_related("meter").filter(meter_id=meter_id)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["meter_pk"] = self.kwargs.get("meter_pk")
        context["fields"] = self.validated_params.get("fields")
        return context

    def filter_queryset(self, queryset):
        self.validated_params = self._validate_params(self.request.query_params)
        filterset = self.filterset_class(data=self.validated_params, queryset=queryset)
        return filterset.qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if not queryset.exists():
            return Response("No data found", status=status.HTTP_204_NO_CONTENT)

        data = pd.DataFrame(queryset)
        if self.validated_params.get("freq"):
            data = self.apply_resample(data)

        serializer = self.get_serializer(data)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def apply_resample(self, df, dropna=True):
        freq = self.validated_params.get("freq")
        agg_func = self.validated_params.get("agg")
        if freq is None or agg_func is None:
            return df

        df_resampled = df.resample(freq, on="collection_date").apply(agg_func)
        df_resampled = df_resampled.reset_index()
        df_resampled = df_resampled.dropna() if dropna else df_resampled.fillna(0)
        return df_resampled

    def _validate_params(self, query_params, raise_exception=True):
        params_serializer = self.query_params_class(data=query_params)
        params_serializer.is_valid(raise_exception=raise_exception)
        return params_serializer.validated_data


@extend_schema(parameters=[DailyProfileParamsValidator])
class DailyProfileViewSet(EnergyChartViewSet):
    queryset = EnergyMeasurement.objects.all()
    filterset_class = DailyProfileFilter
    serializer_class = DailyProfileSerializer
    query_params_class = DailyProfileParamsValidator

    def list(self, request, *args, **kwargs):
        self.validated_params = self._validate_params(self.request.query_params)
        fields = self.validated_params.get("fields")
        detail = self.validated_params.get("detail")

        queryset = self.get_queryset()
        response_data = self._aggregate_data(queryset, fields, detail)
        if not response_data:
            return Response({"detail": "No data found."}, status=status.HTTP_204_NO_CONTENT)
        serializer = self.get_serializer(response_data, many=True, context={"fields": fields})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def _aggregate_data(self, queryset, fields, detail):
        if detail:
            return queryset.quarter_hourly_avg(fields)

        aggregate_qs = queryset.aggregate_hourly("sum", fields, adjust_hour=False)
        if not aggregate_qs:
            return []

        data = pd.DataFrame(list(aggregate_qs))
        grouped_data = data.groupby("hour")[fields].mean().reset_index()
        grouped_data[fields] = grouped_data[fields].astype(float).round(4)
        return grouped_data.to_dict(orient="records")

import logging

from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.renderers import BrowsableAPIRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.devices.models import Meter, Status
from apps.devices.services import collect_meter_data
from apps.measurements.filters import (
    EnergyMeasurementFilter,
    InstantMeasurementFilter,
)
from apps.measurements.models import EnergyMeasurement, InstantMeasurement
from apps.measurements.pagination import MeasurementCursorPagination
from apps.measurements.serializers import (
    DynamicEnergyMeasurementSerializer,
    DynamicInstantMeasurementSerializer,
    EnergyCSVParamsValidator,
    EnergyMeasurementParamsValidator,
    EnergyMeasurementSerializer,
    InstantCSVParamsValidator,
    InstantMeasurementParamsValidator,
    InstantMeasurementSerializer,
    liveInstantMeasurementSerializer,
)
from apps.measurements.services import MeasurementCSVRenderer
from apps.memory_maps.modbus.settings import DATA_GROUP_INSTANT

logger = logging.getLogger("apps.measurements.views")


class InstantMeasurementViewSet(ReadOnlyModelViewSet):
    queryset = InstantMeasurement.objects.all()
    serializer_class = InstantMeasurementSerializer
    filterset_class = InstantMeasurementFilter
    pagination_class = MeasurementCursorPagination
    renderer_classes = [JSONRenderer, MeasurementCSVRenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        queryset = super().get_queryset()

        meter_id = self.kwargs.get("meter_pk")
        if meter_id:
            return queryset.filter(meter_id=meter_id)

        return queryset

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["meter_pk"] = self.kwargs.get("meter_pk")
        context["fields"] = self.validated_params.get("fields")
        return context

    def get_serializer_class(self):
        if self.action == "live":
            return liveInstantMeasurementSerializer
        if self.validated_params.get("fields", None):
            return DynamicInstantMeasurementSerializer
        return InstantMeasurementSerializer

    def filter_queryset(self, queryset):
        self.validated_params = self._validate_params(self.request.query_params)
        filterset = self.filterset_class(data=self.validated_params, queryset=queryset)
        return filterset.qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if not queryset.exists():
            return Response({"detail": "No data found"}, status=status.HTTP_204_NO_CONTENT)

        if request.accepted_renderer.format == "csv":
            return Response(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return Response("Pagination error", status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def _validate_params(self, query_params, raise_exception=True):
        is_csv = self.request.accepted_renderer.format == "csv"
        serializer_class = InstantCSVParamsValidator if is_csv else InstantMeasurementParamsValidator
        params_serializer = serializer_class(data=query_params)

        if not params_serializer.is_valid():
            if is_csv:
                self.request.accepted_renderer = JSONRenderer()
            if raise_exception:
                raise ValidationError(detail=params_serializer.errors)
            return False

        return params_serializer.validated_data

    @action(detail=False, methods=["get"])
    def latest(self, request, meter_pk=None, *args, **kwargs):
        instance = self.get_queryset().values().first()

        if not instance:
            return Response("No data found", status=status.HTTP_204_NO_CONTENT)
        return Response(instance, status=status.HTTP_200_OK)

    @action(detail=False, methods=["get"])
    def live(self, request, meter_pk=None, *args, **kwargs):
        meter = Meter.objects.select_related("model", "model__memory_map").get(pk=meter_pk)

        if meter.current_status.status != Status.ACTIVE:
            raise ValidationError(f"Meter is not active. Current status: {meter.current_status}")

        try:
            collected_data = collect_meter_data(meter, DATA_GROUP_INSTANT)
        except Exception as e:
            logger.error(f"Error collecting data from meter {meter.id}: {str(e)}")
            return Response({"error": str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        serializer = liveInstantMeasurementSerializer(data=collected_data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class EnergyMeasurementViewSet(ReadOnlyModelViewSet):
    queryset = EnergyMeasurement.objects.all()
    serializer_class = EnergyMeasurementSerializer
    filterset_class = EnergyMeasurementFilter
    pagination_class = MeasurementCursorPagination
    renderer_classes = [JSONRenderer, MeasurementCSVRenderer, BrowsableAPIRenderer]

    def get_queryset(self):
        queryset = super().get_queryset()

        meter_id = self.kwargs.get("meter_pk")
        if meter_id:
            return queryset.filter(meter_id=meter_id)

        return queryset

    def get_serializer_class(self):
        if self.validated_params.get("fields"):
            return DynamicEnergyMeasurementSerializer
        return EnergyMeasurementSerializer

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["meter_pk"] = self.kwargs.get("meter_pk")
        context["fields"] = self.validated_params.get("fields")
        return context

    def filter_queryset(self, queryset):
        self.validated_params = self._validate_params(self.request.query_params)
        filterset = self.filterset_class(data=self.validated_params, queryset=queryset)
        return filterset.qs

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        if not queryset.exists():
            return Response("No data found", status=status.HTTP_204_NO_CONTENT)

        if request.accepted_renderer.format == "csv":
            return Response(queryset)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return Response("Pagination error", status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def _validate_params(self, query_params, raise_exception=True):
        is_csv = self.request.accepted_renderer.format == "csv"
        serializer_class = EnergyCSVParamsValidator if is_csv else EnergyMeasurementParamsValidator
        params_serializer = serializer_class(data=query_params)

        if not params_serializer.is_valid():
            if is_csv:
                self.request.accepted_renderer = JSONRenderer()
            if raise_exception:
                raise ValidationError(detail=params_serializer.errors)
            return False

        return params_serializer.validated_data

    @action(detail=False, methods=["get"])
    def latest(self, request, meter_pk=None, *args, **kwargs):
        instance = self.get_queryset().values().first()
        if not instance:
            return Response("No data found", status=status.HTTP_204_NO_CONTENT)
        return Response(instance, status=status.HTTP_200_OK)

import logging

from django.conf import settings
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.mixins import ListModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from apps.devices.models import Meter
from apps.measurements.filters import (
    EnergyMeasurementFilter,
    InstantMeasurementFilter,
)
from apps.measurements.models import EnergyMeasurement, InstantMeasurement
from apps.measurements.serializers import (
    ReportDetailSerializer,
    ReportParamsValidator,
    ReportSummarySerializer,
    UferParamsValidator,
    UferSerializer,
)
from apps.measurements.services import ReportDataAggregator, UferDataAggregator
from apps.organizations.models import Entity

logger = logging.getLogger("apps")


@extend_schema(parameters=[ReportParamsValidator])
class ReportViewSet(ListModelMixin, GenericViewSet):
    queryset = EnergyMeasurement.objects.all()
    serializer_class = ReportSummarySerializer
    filterset_class = EnergyMeasurementFilter

    def get_queryset(self, *args, **kwargs):
        validated_params = getattr(self, "validated_params", None)
        try:
            fields = validated_params.get("fields", [])
            queryset = super().get_queryset().values(*fields, "collection_date", "meter")

        except Exception as e:
            raise ValidationError({"error": str(e)})

        filterset = self.filterset_class(self.validated_params, queryset=queryset)
        return filterset.qs

    def list(self, request, *args, **kwargs):
        self.validated_params = self._validate_params(request, raise_exception=True)
        fields = self.validated_params.get("fields")
        detail = self.validated_params.get("detail")
        entity_id = self.validated_params.get("entity")

        meters_ids = Meter.objects.entity(
            entity=entity_id,
            inc_desc=self.validated_params.get("inc_desc"),
            depth=self.validated_params.get("max_depth"),
        )

        queryset = self.get_queryset().filter(meter__in=meters_ids)
        if not queryset.exists():
            return Response({"detail": "No data found."}, status=status.HTTP_204_NO_CONTENT)

        response_data = self._aggregate_data(queryset, fields, detail)
        agg_fields = self._get_agg_fields(fields)

        if detail:
            serializer = ReportDetailSerializer(data=list(response_data), many=True, context={"fields": agg_fields})
        else:
            serializer = ReportSummarySerializer(data=response_data, context={"fields": agg_fields})

        serializer.is_valid(raise_exception=True)
        response_data = self._build_response_data(queryset, serializer.data, entity_id)
        return Response(response_data, status=status.HTTP_200_OK)

    def _get_agg_fields(self, fields):
        agg_fields = []
        for field in fields:
            agg_fields.extend([f"{field}_peak", f"{field}_off_peak"])
        return agg_fields

    def _aggregate_data(self, queryset, fields, detail):
        aggregator = ReportDataAggregator()
        return aggregator.perform_aggregation(queryset, fields, detail)

    def _validate_params(self, request, raise_exception=True):
        params_serializer = ReportParamsValidator(data=request.query_params)
        params_serializer.is_valid(raise_exception=raise_exception)
        return params_serializer.validated_data

    def _build_response_data(self, queryset, data, entity_id):
        entity = Entity.objects.get(id=entity_id)
        return {
            "Entity": f"{entity.acronym} - {entity.name}",
            "total_measurements": queryset.count(),
            "tariff_peak": settings.TARIFF_PEAK,
            "tariff_off_peak": settings.TARIFF_OFF_PEAK,
            "data": data,
            "info": "Results in kWh",
        }


@extend_schema(parameters=[UferParamsValidator])
class UferViewSet(ListModelMixin, GenericViewSet):
    queryset = InstantMeasurement.objects.all()
    serializer_class = UferSerializer
    filterset_class = InstantMeasurementFilter

    def get_queryset(self, *args, **kwargs):
        validated_params = getattr(self, "validated_params", None)
        try:
            base_fields = ["collection_date", "meter"]
            related_fields = ["meter__ip_address", "meter__located__acronym", "meter__located__name"]
            fields = validated_params.pop("fields")

            queryset = (
                super()
                .get_queryset()
                .select_related("meter", "meter__located")
                .values(*base_fields, *related_fields, *fields)
            )
        except Exception as e:
            raise ValidationError({"error": str(e)})

        filterset = self.filterset_class(self.validated_params, queryset=queryset)
        return filterset.qs

    def list(self, request, *args, **kwargs):
        self.validated_params = self._validate_params(request, raise_exception=True)
        fields = self.validated_params.get("fields")
        threshold_percent = self.validated_params.get("th_percent")
        entity_id = self.validated_params.get("entity")

        meters_ids = Meter.objects.entity(
            entity=entity_id,
            inc_desc=self.validated_params.get("inc_desc"),
            depth=self.validated_params.get("max_depth"),
        )

        queryset = self.get_queryset().filter(meter__in=meters_ids)
        if not queryset.exists():
            return Response({"detail": "No data found."}, status=status.HTTP_204_NO_CONTENT)

        response_data = self._aggregate_data(queryset, fields, threshold_percent)
        serializer = self.get_serializer(data=response_data, many=True, context={"fields": fields})
        serializer.is_valid(raise_exception=True)

        response_data = self._build_response_data(queryset, serializer.data, entity_id, threshold_percent)
        return Response(response_data, status=status.HTTP_200_OK)

    def _aggregate_data(self, queryset, fields, threshold):
        aggregator = UferDataAggregator()
        aggregated_data = aggregator.perform_aggregation(queryset, fields, threshold)
        return [self.process_data(data, fields) for data in aggregated_data]

    def process_data(self, meter_data, fields):
        data = meter_data.copy()
        for field in fields:
            len_total = data[f"{field}_len_total"]
            len_quality = data[f"{field}_len_quality"]
            quality_rate = len_quality / len_total if len_total else 0.0
            data[f"pf_phase_{field[-1]}"] = round(quality_rate * 120, 2)
        return data

    def _validate_params(self, request, raise_exception=True):
        params_serializer = UferParamsValidator(data=request.query_params)
        params_serializer.is_valid(raise_exception=raise_exception)
        return params_serializer.validated_data

    def _build_response_data(self, queryset, data, entity_id, threshold):
        entity = Entity.objects.get(id=entity_id)
        return {
            "Entity": f"{entity.acronym} - {entity.name}",
            "total_measurements": queryset.count(),
            "info": f"Results in (%) above the threshold {threshold}%.",
            "data": data,
        }

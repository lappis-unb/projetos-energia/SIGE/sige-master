import logging

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from apps.devices.models import Meter
from apps.measurements.managers import EnergyMeasurementsManager

logger = logging.getLogger("apps.measurements")


class InstantMeasurement(models.Model):
    meter = models.ForeignKey(Meter, related_name="instant_measurements", on_delete=models.PROTECT)
    frequency_a = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    frequency_b = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    frequency_c = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    frequency_iec = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    voltage_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    voltage_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    voltage_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    voltage_ab = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    voltage_bc = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    voltage_ca = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    current_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    current_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    current_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    active_power_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    active_power_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    active_power_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    total_active_power = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    reactive_power_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    reactive_power_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    reactive_power_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    total_reactive_power = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    apparent_power_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    apparent_power_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    apparent_power_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    total_apparent_power = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    power_factor_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    power_factor_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    power_factor_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    total_power_factor = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    dht_voltage_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    dht_voltage_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    dht_voltage_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    dht_current_a = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    dht_current_b = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    dht_current_c = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    active_demand = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    apparent_demand = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    collection_date = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = _("Instantaneous measurement")
        verbose_name_plural = _("Instantaneous Measurements")
        ordering = ["-collection_date"]
        indexes = [
            models.Index(
                fields=["-collection_date", "meter"],
                name="idx_history_inst_measures",
            ),
        ]

    def __str__(self):
        return f"{self.meter.ip_address} - {self.collection_date}"

    def save(self, *args, **kwargs):
        if self.collection_date is None:
            self.collection_date = timezone.now()
        super().save(*args, **kwargs)


class ReferenceMeasurement(models.Model):
    meter = models.OneToOneField(Meter, related_name="reference_measurement", on_delete=models.PROTECT)
    active_consumption = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    active_generated = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    reactive_inductive = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    reactive_capacitive = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("Reference Measurement")
        verbose_name_plural = _("Reference Measurements")

    def __str__(self):
        return f"{self.meter.ip_address} - {self.updated}"


class EnergyMeasurement(models.Model):
    meter = models.ForeignKey(Meter, related_name="energy_measurements", on_delete=models.PROTECT)
    active_consumption = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    active_generated = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    reactive_inductive = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    reactive_capacitive = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    is_calculated = models.BooleanField(default=False)
    collection_date = models.DateTimeField(null=True, blank=True)

    objects = EnergyMeasurementsManager()

    class Meta:
        verbose_name = _("Energy measurement")
        verbose_name_plural = _("Energy Measurements")
        ordering = ["-collection_date"]
        indexes = [
            models.Index(
                fields=["-collection_date", "meter"],
                name="idx_history_energy_measures",
            ),
        ]

    def __str__(self):
        return f"{self.meter.ip_address} - {self.collection_date}"

    def save(self, *args, **kwargs):
        if self.collection_date is None:
            self.collection_date = timezone.now()
        super().save(*args, **kwargs)

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("measurements", "0004_remove_cumulativemeasurement_cumu_transductor_date_idx_and_more"),
        ("devices", "0002_rename_transductor_to_meter_tables"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="CumulativeMeasurement",
            new_name="EnergyMeasurement",
        ),
        migrations.AlterModelOptions(
            name="energymeasurement",
            options={
                "ordering": ["-collection_date"],
                "verbose_name": "Energy measurement",
                "verbose_name_plural": "Energy Measurements",
            },
        ),
        migrations.AlterField(
            model_name="energymeasurement",
            name="collection_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="instantmeasurement",
            name="collection_date",
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.RenameField(
            model_name="energymeasurement",
            old_name="transductor",
            new_name="meter",
        ),
        migrations.RenameField(
            model_name="instantmeasurement",
            old_name="transductor",
            new_name="meter",
        ),
        migrations.RenameField(
            model_name="referencemeasurement",
            old_name="transductor",
            new_name="meter",
        ),
        migrations.AddIndex(
            model_name="energymeasurement",
            index=models.Index(fields=["-collection_date", "meter"], name="idx_history_energy_measures"),
        ),
        migrations.AddIndex(
            model_name="instantmeasurement",
            index=models.Index(fields=["-collection_date", "meter"], name="idx_history_inst_measures"),
        ),
    ]

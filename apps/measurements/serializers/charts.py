from django.utils import timezone
from drf_spectacular.utils import extend_schema_serializer
from rest_framework import serializers

from apps.measurements.serializers.utils import daily_profile_example, graph_data_example


@extend_schema_serializer(examples=[daily_profile_example])
class DailyProfileSerializer(serializers.Serializer):
    time = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_dynamic_fields()

    def add_dynamic_fields(self):
        fields = self.context.get("fields", [])
        if fields:
            for field in fields:
                self.fields[field] = serializers.FloatField()

    def get_time(self, obj):
        hour = obj.get("hour", 0)
        minute = obj.get("minute", 0)
        second = obj.get("second", 0)
        return f"{hour:02}:{minute:02}:{second:02}"


@extend_schema_serializer(examples=[graph_data_example])
class ChartMeasurementSerializer(serializers.Serializer):
    metadata = serializers.SerializerMethodField()
    timestamps = serializers.ListField(child=serializers.CharField(), allow_null=True)
    traces = serializers.ListField(child=serializers.DictField(), allow_null=True)

    def get_metadata(self, instance):
        tz = timezone.get_current_timezone()
        return {
            "points": len(instance),
            "start_date": instance.collection_date.min().astimezone(tz),
            "end_date": instance.collection_date.max().astimezone(tz),
            "time_zone": f"{tz.key} (UTC{tz.tzname(instance.collection_date.min())})",
            "meter": self.context.get("meter_pk"),
            "fields": self.context.get("fields", []),
        }

    def to_representation(self, instance):
        rep = super().to_representation(instance)
        tz = timezone.get_current_timezone()
        timestamps = instance.collection_date.apply(lambda ts: ts.astimezone(tz))
        rep["timestamps"] = timestamps.tolist()

        traces = []
        fields = self.context.get("fields")
        for field in fields:
            field_data = {
                "field": field,
                "avg_value": instance[field].mean().round(2),
                "max_value": instance[field].max(),
                "min_value": instance[field].min(),
                "data": instance[field].tolist(),
            }
            traces.append(field_data)
        rep["traces"] = traces
        return rep

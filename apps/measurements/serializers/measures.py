import logging

from django.utils import timezone
from rest_framework import serializers

from apps.measurements.models import (
    EnergyMeasurement,
    InstantMeasurement,
    ReferenceMeasurement,
)
from apps.measurements.services import EnergyMeasurementManager

logger = logging.getLogger("apps")


class InstantMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstantMeasurement
        fields = "__all__"

        extra_kwargs = {"collection_date": {"read_only": True}}


class DynamicInstantMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstantMeasurement
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        required_fields = {"id", "trasductor", "collection_date"}
        dynamic_fields = set(self.context.get("fields", []))
        allowed_fields = required_fields | dynamic_fields

        for field_name in list(self.fields.keys()):
            if field_name not in allowed_fields:
                self.fields.pop(field_name)


class liveInstantMeasurementSerializer(serializers.ModelSerializer):
    collection_date = serializers.DateTimeField(default=timezone.now)

    class Meta:
        model = InstantMeasurement
        fields = [
            "frequency_a",
            "frequency_b",
            "frequency_c",
            "voltage_a",
            "voltage_b",
            "voltage_c",
            "voltage_ab",
            "voltage_bc",
            "voltage_ca",
            "current_a",
            "current_b",
            "current_c",
            "active_power_a",
            "active_power_b",
            "active_power_c",
            "total_active_power",
            "reactive_power_a",
            "reactive_power_b",
            "reactive_power_c",
            "total_reactive_power",
            "apparent_power_a",
            "apparent_power_a",
            "apparent_power_a",
            "total_apparent_power",
            "power_factor_a",
            "power_factor_b",
            "power_factor_c",
            "total_power_factor",
            "collection_date",
        ]
        read_only = True


class ReferenceMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnergyMeasurement
        fields = [
            "id",
            "meter",
            "active_consumption",
            "active_generated",
            "reactive_inductive",
            "reactive_capacitive",
            "created",
            "updated",
        ]
        extra_kwargs = {"created": {"read_only": True}, "updated": {"read_only": True}}


class EnergyMeasurementSerializer(serializers.ModelSerializer):
    collection_date = serializers.DateTimeField(default=timezone.now)

    class Meta:
        model = EnergyMeasurement
        fields = [
            "id",
            "meter",
            "active_consumption",
            "active_generated",
            "reactive_inductive",
            "reactive_capacitive",
            "is_calculated",
            "collection_date",
        ]

    def create(self, validated_data):
        meter = validated_data.get("meter")
        last_measurement, created = ReferenceMeasurement.objects.get_or_create(meter=meter)
        fields = self.get_energy_fields()

        if created:
            data = validated_data.copy()
            data.update({field: 0 for field in fields})
            energy_measurements = super().create(data)
        else:
            energy_measurements = EnergyMeasurementManager.handle_measurements(
                validated_data,
                last_measurement,
                fields,
            )

        try:
            self.update_reference_measurement(last_measurement, validated_data, fields)
        except Exception as e:
            logger.error(f"Error updating reference measurement: {e}")
            raise ValueError("Error updating reference measurement")

        return energy_measurements

    def update_reference_measurement(self, last_measurement, validated_data, fields):
        for field in fields:
            if field in validated_data:
                setattr(last_measurement, field, validated_data[field])
        last_measurement.save()

    def get_energy_fields(self):
        return ["active_consumption", "active_generated", "reactive_inductive", "reactive_capacitive"]


class DynamicEnergyMeasurementSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnergyMeasurement
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        required_fields = {"id", "trasductor", "collection_date"}
        dynamic_fields = set(self.context.get("fields", []))
        allowed_fields = required_fields | dynamic_fields

        for field_name in list(self.fields.keys()):
            if field_name not in allowed_fields:
                self.fields.pop(field_name)

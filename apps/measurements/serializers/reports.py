import logging

from drf_spectacular.utils import extend_schema_serializer
from rest_framework import serializers

from apps.devices.models import Meter
from apps.measurements.serializers.utils import (
    energy_report_example,
    ufer_report_example,
)

logger = logging.getLogger("apps.measurements.serializers")


@extend_schema_serializer(examples=[ufer_report_example])
class UferSerializer(serializers.Serializer):
    meter = serializers.IntegerField()
    located_name = serializers.CharField()
    located_acronym = serializers.CharField()
    ip_address = serializers.CharField()
    total_measurements = serializers.IntegerField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        fields = self.context.get("fields", [])
        for field in fields:
            self.fields[f"pf_phase_{field[-1]}"] = serializers.FloatField(required=False)


@extend_schema_serializer(examples=[energy_report_example])
class ReportSummarySerializer(serializers.Serializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_dynamic_fields()

    def add_dynamic_fields(self):
        fields = self.context.get("fields", [])
        if fields:
            for field in fields:
                self.fields[field] = serializers.FloatField()


@extend_schema_serializer(examples=[energy_report_example])
class ReportDetailSerializer(serializers.Serializer):
    meter = serializers.IntegerField()
    total_measurements = serializers.IntegerField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_dynamic_fields()

    def add_dynamic_fields(self):
        fields = self.context.get("fields", [])
        if fields:
            for field in fields:
                self.fields[field] = serializers.FloatField()

    def to_representation(self, instance):
        meter = (
            Meter.objects.filter(pk=instance["meter"])
            .values("located__acronym", "located__name", "ip_address")
            .first()
        )
        rep = {
            "located": f"{meter['located__acronym']} - {meter['located__name']}",
            "ip_address": meter["ip_address"],
        }
        rep.update(super().to_representation(instance))
        return rep

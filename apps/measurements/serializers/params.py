import logging

import pandas as pd
from django.conf import settings
from rest_framework import serializers
from rest_framework.serializers import ValidationError

from apps.common.serializers import DateRangeParamsValidator
from apps.common.utils import field_params
from apps.measurements.models import EnergyMeasurement, InstantMeasurement

logger = logging.getLogger("apps.measurements.serializers")


class BaseMeasurementParamsValidator(DateRangeParamsValidator):
    fields = serializers.CharField(required=False, **field_params("fields"))

    class Meta(DateRangeParamsValidator.Meta):
        params = DateRangeParamsValidator.Meta.params + ["fields"]
        required_params = []

    def validate_fields(self, value: str):
        if value == "all":
            return self.Meta.model_allowed_fields

        fields = [field for field in value.split(",") if field]
        allowed_fields = self.Meta.model_allowed_fields
        invalid_fields = set(fields) - set(allowed_fields)

        if invalid_fields:
            raise ValidationError(
                f"Invalid fields: {', '.join(invalid_fields)}.  "
                f"Choices are: {', '.join(self.Meta.model_allowed_fields)}."
            )
        return fields


class InstantMeasurementParamsValidator(BaseMeasurementParamsValidator):
    class Meta(BaseMeasurementParamsValidator.Meta):
        model_allowed_fields = [
            str(field.name)
            for field in InstantMeasurement._meta.fields
            if field.name not in {"id", "collection_date", "meter"}
        ]


class EnergyMeasurementParamsValidator(BaseMeasurementParamsValidator):
    class Meta(BaseMeasurementParamsValidator.Meta):
        model_allowed_fields = [
            str(field.name)
            for field in EnergyMeasurement._meta.fields
            if field.name not in {"id", "collection_date", "meter", "is_calculated"}
        ]


class InstantCSVParamsValidator(InstantMeasurementParamsValidator):
    class Meta(InstantMeasurementParamsValidator.Meta):
        required_params = ["fields"]
        max_period_days = settings.MAX_DAYS_INSTANT_EXPORT

    def validate(self, attrs):
        attrs = super().validate(attrs)

        if not (attrs.get("start_date") or attrs.get("last")):
            raise serializers.ValidationError("Missing required parameter: period 'start_date/end_date' or 'last'")
        return attrs


class EnergyCSVParamsValidator(EnergyMeasurementParamsValidator):
    class Meta(EnergyMeasurementParamsValidator.Meta):
        required_params = ["fields"]
        max_period_days = settings.MAX_DAYS_ENERGY_EXPORT

    def validate(self, attrs):
        attrs = super().validate(attrs)

        if not (attrs.get("start_date") or attrs.get("last")):
            raise serializers.ValidationError({"period": "You must provide either 'start_date' or 'last' parameter"})
        return attrs


class InstantChartParamsValidator(InstantMeasurementParamsValidator):
    lttb = serializers.BooleanField(required=False, **field_params("lttb"))
    threshold = serializers.IntegerField(required=False, min_value=2, **field_params("threshold"))
    only_day = serializers.BooleanField(required=False, **field_params("only_day"))

    class Meta(InstantMeasurementParamsValidator.Meta):
        params = InstantMeasurementParamsValidator.Meta.params + ["lttb", "threshold", "only_day"]
        required_params = ["fields"]

    def validate(self, attrs):
        attrs = super().validate(attrs)

        if not (attrs.get("start_date") or attrs.get("last")):
            raise serializers.ValidationError("Missing required parameter: period 'start_date/end_date' or 'last'")
        return attrs


class EnergyChartParamsValidator(EnergyMeasurementParamsValidator):
    freq = serializers.CharField(required=False, **field_params("freq"))
    agg = serializers.CharField(required=False, **field_params("agg"))
    only_day = serializers.BooleanField(required=False, **field_params("only_day"))

    class Meta(EnergyMeasurementParamsValidator.Meta):
        params = EnergyMeasurementParamsValidator.Meta.params + ["freq", "agg", "only_day"]
        required_params = ["fields"]

    def validate_freq(self, value):
        try:
            time_delta = pd.to_timedelta(value)
        except ValueError:
            raise ValidationError("Invalid frequency format. Use ISO 8601 duration format.")

        if time_delta.total_seconds() < 900:
            raise ValidationError("Invalid frequency. Use a minimum of 15 minutes.")
        return time_delta

    def validate_agg(self, value):
        if value not in ["sum", "mean", "max", "min"]:
            raise ValidationError("Invalid aggregation function. Use: 'sum', 'mean', 'max' or 'min'.")
        return value

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if bool(attrs.get("freq")) ^ bool(attrs.get("agg")):
            raise ValidationError("You must provide 'freq' and 'agg' parameters together.")
        return attrs


class UferParamsValidator(InstantMeasurementParamsValidator):
    meter = serializers.IntegerField(min_value=1, required=False, **field_params("meter"))
    entity = serializers.IntegerField(min_value=1, **field_params("entity"))
    inc_desc = serializers.BooleanField(required=False, **field_params("inc_desc"))
    max_depth = serializers.IntegerField(required=False, min_value=0, **field_params("depth"))
    only_day = serializers.BooleanField(required=False, **field_params("only_day"))
    th_percent = serializers.IntegerField(min_value=1, max_value=100, required=False, **field_params("th_percent"))

    class Meta(InstantMeasurementParamsValidator.Meta):
        params = InstantMeasurementParamsValidator.Meta.params + [
            "meter",
            "entity",
            "inc_desc",
            "max_depth",
            "only_day",
            "th_percent",
        ]
        required_params = ["entity"]
        model_allowed_fields = ["power_factor_a", "power_factor_b", "power_factor_c"]
        max_period_days = settings.MAX_DAYS_ENERGY_EXPORT

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if not attrs.get("fields"):
            attrs["fields"] = self.Meta.model_allowed_fields

        if not (attrs.get("start_date") or attrs.get("last")):
            raise serializers.ValidationError("Missing required parameter: period 'start_date/end_date' or 'last'")
        return attrs


class DailyProfileParamsValidator(EnergyMeasurementParamsValidator):
    detail = serializers.BooleanField(required=False, **field_params("detail_profile"))
    peak_hours = serializers.BooleanField(required=False, **field_params("peak_hours"))
    off_peak_hours = serializers.BooleanField(required=False, **field_params("off_peak_hours"))

    class Meta(EnergyMeasurementParamsValidator.Meta):
        params = EnergyMeasurementParamsValidator.Meta.params + [
            "detail",
            "peak_hours",
            "off_peak_hours",
        ]
        required_params = ["fields"]

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if attrs.get("peak_hours") and attrs.get("off_peak_hours"):
            raise ValidationError("You can't use 'peak_hours' and 'off_peak_hours' parameters together.")
        return attrs


class ReportParamsValidator(EnergyMeasurementParamsValidator):
    meter = serializers.IntegerField(min_value=1, required=False, **field_params("meter"))
    entity = serializers.IntegerField(min_value=1, **field_params("entity"))
    inc_desc = serializers.BooleanField(**field_params("inc_desc"))
    only_day = serializers.BooleanField(required=False, **field_params("only_day"))
    depth = serializers.IntegerField(min_value=0, required=False, **field_params("depth"))
    detail = serializers.BooleanField(**field_params("detail_report"))

    class Meta(EnergyMeasurementParamsValidator.Meta):
        params = EnergyMeasurementParamsValidator.Meta.params + [
            "meter",
            "entity",
            "inc_desc",
            "only_day",
            "depth",
            "detail",
        ]
        required_params = ["entity"]
        max_period_days = settings.MAX_DAYS_ENERGY_EXPORT

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if not attrs.get("fields"):
            attrs["fields"] = self.Meta.model_allowed_fields

        if not (attrs.get("start_date") or attrs.get("last")):
            raise serializers.ValidationError("Missing required parameter: period 'start_date/end_date' or 'last'")
        return attrs

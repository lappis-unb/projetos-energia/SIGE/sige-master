from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter

from apps.devices.urls import router as meters_base_router
from apps.measurements.views import (
    DailyProfileViewSet,
    EnergyChartViewSet,
    EnergyMeasurementViewSet,
    InstantChartViewSet,
    InstantMeasurementViewSet,
    ReportViewSet,
    UferViewSet,
)

app_name = "measurements"

router = DefaultRouter()
router.register(r"instant/measurements", InstantMeasurementViewSet, basename="instant-measurements")
router.register(r"energy/measurements", EnergyMeasurementViewSet, basename="energy-measurements")
router.register(r"instant/reports/ufer", UferViewSet, basename="report-ufer")
router.register(r"energy/reports", ReportViewSet, basename="report-energy")

meter_measurements_router = NestedDefaultRouter(meters_base_router, r"meters", lookup="meter")
meter_measurements_router.register(r"instant/measurements", InstantMeasurementViewSet, basename="meter-instant")
meter_measurements_router.register(r"energy/measurements", EnergyMeasurementViewSet, basename="meter-energy")
meter_measurements_router.register(r"instant/charts", InstantChartViewSet, basename="meter-instant-chart")
meter_measurements_router.register(r"energy/charts", EnergyChartViewSet, basename="meter-energy-chart")
meter_measurements_router.register(r"energy/charts/daily-profile", DailyProfileViewSet, basename="meter-daily-profile")


urlpatterns = [
    path("", include(router.urls)),
    path("", include(meter_measurements_router.urls)),
]


from rest_framework.pagination import PageNumberPagination

from apps.common.pagination import BaseCursorPagination


class MeasurementCursorPagination(BaseCursorPagination):
    cursor_query_param = "cursor"
    page_size = 20
    max_page_size = 1000
    ordering = "-collection_date"
    page_size_query_param = "page_size"
    offset_cutoff = 1000


class MeasurementPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = "page_size"
    max_page_size = 1000
    last_page_strings = ("last",)

import logging
from datetime import timedelta

from django.db.models import Count, Max, Min, Q, Sum
from django.db.models.functions import Coalesce
from django.utils import timezone

from apps.devices.models import Status
from apps.memory_maps.modbus.data_reader import ModbusClientFactory
from apps.memory_maps.modbus.settings import DATA_GROUPS

SECOND_TO_HOUR = 3600

logger = logging.getLogger("tasks")


def collect_meter_data(meter, data_group):
    if data_group not in DATA_GROUPS:
        logger.error(f"Unknown data_group: {data_group}")

    memory_map = meter.model.memory_map
    register_blocks = memory_map.get_memory_map_by_type(data_group)

    modbus_collector = ModbusClientFactory(
        ip_address=meter.ip_address,
        port=meter.port,
        slave_id=meter.model.modbus_addr_id,
        method=meter.model.protocol,
    )

    try:
        collected_data = modbus_collector.read_datagroup_blocks(register_blocks)
        collected_data["meter"] = meter.id
        return collected_data
    except Exception as e:
        logger.error(f"Error reading from meter {meter.id}: {str(e)}")
        raise


class MeterStatusAnalytics:
    def __init__(self, status_qs, meter_qs=None):
        self.status_qs = status_qs
        self.meter_qs = meter_qs

    def get_general_metrics(self):
        return self._calculate_metrics(self.status_qs)

    def get_meter_metrics(self, meter):
        queryset = self.status_qs.filter(meter=meter)
        return self._calculate_metrics(queryset)

    def _calculate_metrics(self, queryset):
        return queryset.aggregate(
            **self._get_base_anotations(),
            **self._get_time_metrics(),
        )

    def _get_base_anotations(self):
        return {
            "total_meters": Count("meter", distinct=True),
            "broken": Count("id", filter=Q(status=Status.BROKEN)),
            "time_broken": Coalesce(Sum("duration", filter=Q(status=Status.BROKEN)), timedelta()),
            "time_maintenance": Coalesce(Sum("duration", filter=Q(status=Status.MAINTENANCE)), timedelta()),
            "time_disabled": Coalesce(Sum("duration", filter=Q(status=Status.DISABLED)), timedelta()),
            "first_occurrence": Min("start_time"),
            "last_occurrence": Max("start_time"),
        }

    def _get_time_metrics(self):
        return {
            "downtime": Coalesce(Sum("duration", filter=Q(status=Status.BROKEN)), timedelta()),
            "uptime": Coalesce(Sum("duration", filter=Q(status=Status.ACTIVE)), timedelta()),
            "longest_downtime": Max("duration", filter=Q(status=Status.BROKEN)),
            "longest_uptime": Max("duration", filter=Q(status=Status.ACTIVE)),
            "total_time": Sum("duration"),
        }

    def _calculate_availability_metrics(self, metrics):
        total_seconds = metrics["total_time"].total_seconds() if metrics["total_time"] else 0
        if not total_seconds:
            return self._get_zero_metrics()

        uptime_hours = round(metrics["uptime"].total_seconds() / SECOND_TO_HOUR, 2)
        downtime_hours = round(metrics["downtime"].total_seconds() / SECOND_TO_HOUR, 2)
        total_hours = total_seconds / SECOND_TO_HOUR

        failure_count = metrics["broken"]
        if failure_count > 0:
            mtbf = uptime_hours / failure_count
            mttr = downtime_hours / failure_count
            failure_rate = failure_count / total_hours
        else:
            mtbf = total_hours
            mttr = 0

        availability_rate = (uptime_hours / total_hours) * 100
        reliability_rate = (mtbf / total_hours) * 100

        return {
            "uptime_hours": uptime_hours,
            "uptime_rate": uptime_hours / total_hours,
            "downtime_hours": downtime_hours,
            "downtime_rate": downtime_hours / total_hours,
            "mtbf_hours": round(mtbf, 2),
            "mttr_hours": round(mttr, 2),
            "availability_rate": round(availability_rate, 2),
            "reliability_rate": round(reliability_rate, 2),
            "failure_rate": round(failure_rate, 2),
        }

    def _get_zero_metrics(self):
        return {
            "uptime_hours": 0,
            "uptime_rate": 0,
            "downtime_hours": 0,
            "downtime_rate": 0,
            "mtbf_hours": 0,
            "mttr_hours": 0,
            "availability_rate": 0,
            "reliability_rate": 0,
            "failure_rate": 0,
        }


class MeterStatusReports:
    def __init__(self, status_analytics, validated_params):
        self.analytics = status_analytics
        self.start_date = validated_params.get("start_date", None)
        self.end_date = validated_params.get("end_date", None)

    def get_general_report(self):
        metrics = self.analytics.get_general_metrics()
        initial_data = {"total_meters": metrics["total_meters"]}
        return self._build_summary(metrics, initial_data)

    def get_meter_report(self, meter):
        metrics = self.analytics.get_meter_metrics(meter)
        initial_data = {"meter": meter.id}
        return self._build_summary(metrics, initial_data, meter)

    def _build_summary(self, metrics, initial_data, meter=None):
        availability = self.analytics._calculate_availability_metrics(metrics)

        summary = {
            "period": self._get_period(metrics),
            "overview": self._get_overview(metrics),
            "availability": self._format_availability_metrics(availability),
            "reliability": self._format_reliability_metrics(availability),
        }

        if meter:
            initial_data["current_status"] = self._get_current_status(meter)

        return {**initial_data, **summary}

    def _get_period(self, metrics):
        if self.start_date:
            return {
                "start_date": self.start_date,
                "end_date": self.end_date or timezone.now(),
            }

        if metrics["broken"] > 0:
            return {
                "start_date": metrics["first_occurrence"],
                "end_date": timezone.now(),
            }
        return None

    def _get_overview(self, metrics):
        active_hours = metrics["uptime"].total_seconds() / SECOND_TO_HOUR
        broken_hours = metrics["time_broken"].total_seconds() / SECOND_TO_HOUR
        maintenance_hours = metrics["time_maintenance"].total_seconds() / SECOND_TO_HOUR
        disabled_hours = metrics["time_disabled"].total_seconds() / SECOND_TO_HOUR
        return {
            "total_failures": metrics["broken"],
            "active_hours": round(active_hours, 2),
            "broken_hours": round(broken_hours, 2),
            "maintenance_hours": round(maintenance_hours, 2),
            "disabled_hours": round(disabled_hours, 2),
        }

    def _get_current_status(self, meter):
        current = meter.current_status
        duration_hours = (timezone.now() - current.start_time).total_seconds() / SECOND_TO_HOUR
        return {
            "state": current.get_status_display(),
            "since": current.start_time,
            "duration_hours": round(duration_hours, 2),
            "notes": current.notes,
        }

    def _format_availability_metrics(self, availability):
        return {
            "availability_rate": availability["availability_rate"],
            "uptime_hours": availability["uptime_hours"],
            "downtime_hours": availability["downtime_hours"],
        }

    def _format_reliability_metrics(self, availability):
        return {
            "reliability_rate": availability["reliability_rate"],
            "failure_rate": availability["failure_rate"],
            "mtbf_hours": availability["mtbf_hours"],
            "mttr_hours": availability["mttr_hours"],
        }

    def _format_meter_info(self, meter):
        return {
            "id": meter.id,
            "ip_address": meter.ip_address,
            "model": meter.model.name,
            "location": meter.located.name,
        }

import logging

from django.core.validators import MaxValueValidator
from django.db import IntegrityError, transaction
from rest_framework import serializers
from rest_framework.serializers import ValidationError

from apps.devices.models import (
    Meter,
    MeterModel,
    Status,
    StatusHistory,
)
from apps.devices.utils import parse_uploaded_csv_file
from apps.devices.validators import CsvFileValidator
from apps.locations.models import GeographicLocation
from apps.locations.serializers import BasicGeographicLocationSerializer
from apps.memory_maps.models import MemoryMap
from apps.organizations.serializers import EntityDetailSerializer

from .status import MeterStatusSerializer

logger = logging.getLogger("apps")


class MeterModelSerializer(serializers.ModelSerializer):
    memory_map_file = serializers.FileField(validators=[CsvFileValidator()])
    max_block_size = serializers.IntegerField(validators=[MaxValueValidator(125)])
    read_function = serializers.SerializerMethodField()
    protocol = serializers.SerializerMethodField()

    class Meta:
        model = MeterModel
        fields = [
            "id",
            "manufacturer",
            "name",
            "protocol",
            "read_function",
            "modbus_addr_id",
            "max_block_size",
            "base_address",
            "notes",
            "memory_map_file",
        ]

    def get_read_function(self, obj):
        return obj.get_read_function_display()

    def get_protocol(self, obj):
        return obj.get_protocol_display()

    def validate_memory_map_file(self, value):
        if not value:
            return value

    def validate_max_block_size(self, value):
        if value > 125:
            raise serializers.ValidationError(
                "The maximum number of contiguous registers that can be read using pymodbus is 125"
            )
        return value

    def create(self, validated_data):
        """
        Responsibility to create the instance of MeterModel and creating related objects (MemoryMap)
        based on data passed in through the validated_data object (csv_map).
        """

        try:
            with transaction.atomic():
                csv_data = parse_uploaded_csv_file(validated_data["memory_map_file"])
                meter_model = super().create(validated_data)

                MemoryMap.create_from_csv(
                    meter_model,
                    csv_data,
                    validated_data["max_block_size"],
                )
                return meter_model
        except (IntegrityError, ValidationError) as e:
            raise ValidationError(f"An exception of type {type(e).__name__} occurred: {e}")

    def update(self, instance, validated_data):
        csv_data = parse_uploaded_csv_file(validated_data["memory_map_file"])

        try:
            instance.memory_map.update_from_csv(
                csv_data,
                validated_data["max_block_size"],
            )
        except Exception as e:
            raise ValidationError(f"An exception of type {type(e).__name__} occurred: {e}")
        return super().update(instance, validated_data)


class MeterCreateSerializer(serializers.ModelSerializer):
    port = serializers.IntegerField(validators=[MaxValueValidator(65535)])
    status = serializers.ChoiceField(choices=Status.choices, write_only=True, required=True)
    geo_location = BasicGeographicLocationSerializer()

    class Meta:
        model = Meter
        fields = [
            "id",
            "model",
            "is_generator",
            "located",
            "serial_number",
            "ip_address",
            "port",
            "firmware_version",
            "status",
            "installation_date",
            "geo_location",
        ]

    def create(self, validated_data):
        initial_status = validated_data.pop("status")
        notes = f"Meter created with status: {Status(initial_status).label}"
        geo_location_data = validated_data.pop("geo_location")

        try:
            with transaction.atomic():
                geo_location = GeographicLocation.objects.create(**geo_location_data)
                meter = Meter.objects.create(geo_location=geo_location, **validated_data)

                StatusHistory.objects.create(
                    meter=meter,
                    status=initial_status,
                    notes=notes,
                )
            return meter
        except IntegrityError as e:
            raise ValidationError(f"An exception of type {type(e).__name__} occurred: {e}")


class MeterListSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    located = serializers.SerializerMethodField()
    model = serializers.SerializerMethodField()

    class Meta:
        model = Meter
        fields = [
            "id",
            "model",
            "ip_address",
            "port",
            "status",
            "located",
            "is_generator",
        ]

    def get_located(self, instance):
        return f"{instance.located.name} ({instance.located.acronym})"

    def get_model(self, instance):
        return f"{instance.model.manufacturer} {instance.model.name}"

    def get_status(self, instance):
        return instance.current_status.get_status_display()


class MeterDetailSerializer(serializers.ModelSerializer):
    model = serializers.SerializerMethodField()
    current_status = MeterStatusSerializer()
    located = EntityDetailSerializer()
    geo_location = BasicGeographicLocationSerializer()

    class Meta:
        model = Meter
        fields = [
            "id",
            "serial_number",
            "ip_address",
            "port",
            "is_generator",
            "model",
            "located",
            "geo_location",
            "current_status",
            "installation_date",
            "firmware_version",
            "description",
        ]

    def get_model(self, instance):
        return {
            "name": f"{instance.model.manufacturer} {instance.model.name}",
            "read_function": instance.model.get_read_function_display(),
            "protocol": instance.model.get_protocol_display(),
        }

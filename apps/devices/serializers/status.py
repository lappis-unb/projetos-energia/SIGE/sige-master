import logging

from rest_framework import serializers

from apps.devices.models import StatusHistory

logger = logging.getLogger("apps")


class MeterStatusSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source="get_status_display")

    class Meta:
        model = StatusHistory
        fields = [
            "id",
            "status",
            "start_time",
            "notes",
        ]


class MeterStatusDetailSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source="get_status_display")
    meter = serializers.SerializerMethodField()
    model = serializers.SerializerMethodField()

    class Meta:
        model = StatusHistory
        fields = [
            "id",
            "meter",
            "model",
            "status",
            "start_time",
            "end_time",
            "duration",
            "notes",
        ]

    def get_meter(self, instance):
        return instance.meter.ip_address

    def get_model(self, instance):
        return f"{instance.meter.model.manufacturer} {instance.meter.model.name}"

import logging

import pandas as pd

from rest_framework import serializers
from rest_framework.serializers import ValidationError

from apps.common.serializers import BaseParamsValidator, DateRangeParamsValidator
from apps.devices.models import Status

logger = logging.getLogger("apps.devices.serializers")


class StatusValidatorMixin:
    def validate_status(self, value):
        if value.isdigit() and int(value) in Status.values:
            return int(value)
        if value.upper() in Status.names:
            return Status[value.upper()].value
        for status in Status:
            if status.label.upper() == value.upper():
                return status.value
        raise ValidationError(
            f"Invalid status '{value}'. Use: {', '.join(f'{s.value} ({s.label.lower()})' for s in Status)}"
        )


class MeterParamsValidator(BaseParamsValidator, StatusValidatorMixin):
    entity = serializers.IntegerField(required=False, min_value=1)
    status = serializers.CharField(required=False)
    is_generator = serializers.BooleanField(required=False, allow_null=True)

    class Meta:
        model = Status
        params = ["entity", "status", "is_generator"]


class StatusHistoryParamsValidator(DateRangeParamsValidator, StatusValidatorMixin):
    status = serializers.CharField(required=False)
    duration_gt = serializers.CharField(required=False)
    duration_lt = serializers.CharField(required=False)

    class Meta(DateRangeParamsValidator.Meta):
        model = Status
        params = ["start_date", "end_date", "last", "status", "duration_gt", "duration_lt"]
        # max_period_days = 13

    def validate_duration_gt(self, value):
        return self._validate_duration(value)

    def validate_duration_lt(self, value):
        return self._validate_duration(value)

    def _validate_duration(self, value):
        try:
            duration = pd.Timedelta(value).total_seconds()
            if duration < 0:
                raise ValidationError(f"Invalid value: {value}. Must be greater than 0")
            return duration
        except ValueError:
            raise ValidationError(
                f"Invalid format: '{value}'. Use format: '<number><unit>' (e.g., '60s', '15m', '12h', '7d')."
            )

    def validate(self, attrs):
        attrs = super().validate(attrs)
        duration_gt = attrs.get("duration_gt")
        duration_lt = attrs.get("duration_lt")

        if duration_gt and duration_lt and duration_gt > duration_lt:
            raise ValidationError({"duration": "Invalid range: duration_gt must be less than duration_lt."})
        return attrs

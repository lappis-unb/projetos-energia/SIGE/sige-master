from django_filters import rest_framework as filters

from apps.organizations.models import Entity


class MeterFilterSet(filters.FilterSet):
    is_generator = filters.BooleanFilter(field_name="is_generator", lookup_expr="exact")
    entity = filters.NumberFilter(method="filter_entity")
    status = filters.CharFilter(method="filter_status")

    def filter_status(self, queryset, name, value):
        return queryset.filter(status_history__status=value, status_history__end_time__isnull=True)

    def filter_entity(self, queryset, name, value):
        entity = Entity.objects.get(pk=value)
        return queryset.filter(located__in=entity.get_descendants(include_self=True))

    class Meta:
        fields = ["entity", "status", "is_generator"]


class StatusSummaryFilterSet(filters.FilterSet):
    start_date = filters.DateTimeFilter(field_name="start_time", lookup_expr="gte")
    end_date = filters.DateTimeFilter(field_name="start_time", lookup_expr="lte")

    class Meta:
        fields = ["start_date", "end_date"]


class StatusHistoryFilterSet(filters.FilterSet):
    start_date = filters.DateTimeFilter(field_name="start_time", lookup_expr="gte")
    end_date = filters.DateTimeFilter(field_name="start_time", lookup_expr="lte")
    status = filters.CharFilter(field_name="status", lookup_expr="exact")
    duration_gt = filters.DurationFilter(field_name="duration", lookup_expr="gt")
    duration_lt = filters.DurationFilter(field_name="duration", lookup_expr="lt")

    class Meta:
        fields = ["start_date", "end_date", "status", "duration_gt", "duration_lt"]

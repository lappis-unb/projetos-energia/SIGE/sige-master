from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('devices', '0001_initial'),
        ('measurements', '0001_initial'), 
        ('memory_maps', '0001_initial'),
        ('events', '0006_transductorstatustrigger'), 
    ]
    
    operations = [
        migrations.RenameModel(
            old_name='TransductorModel',
            new_name='MeterModel',
        ),
        migrations.RenameModel(
            old_name='Transductor',
            new_name='Meter',
        ),
    ]
    
import django.db.models.deletion
import django.utils.timezone
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('devices', '0002_rename_transductor_to_meter_tables'),
    ]
    
    operations = [
        migrations.RenameField(
            model_name='statushistory',
            old_name='transductor',
            new_name='meter',
        ),
        migrations.AlterField(
            model_name="meter",
            name="model",
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to="devices.metermodel"),
        ),
        migrations.AlterField(
            model_name="statushistory",
            name="meter",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT, 
                related_name="status_history", 
                to="devices.meter"
            ),
        ),
        migrations.AlterField(
            model_name='meter',
            name='geo_location',
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.PROTECT,
                related_name='meter',
                to='locations.geographiclocation'
            ),
        ),
        migrations.AlterField(
            model_name='meter',
            name='located',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name='meters',
                to='organizations.entity'
            ),
        ),  
        migrations.AddIndex(
            model_name='statushistory',
            index=models.Index(fields=['-start_time', 'meter_id'], name='idx_status_time_meter'),
        ),
        migrations.AlterModelOptions(
            name='meter',
            options={
                'ordering': ('model', 'ip_address'),
                'verbose_name': 'meter',
                'verbose_name_plural': 'meters'
            },
        ),
        migrations.AlterModelOptions(
            name='statushistory',
            options={'ordering': ['-start_time'], 'verbose_name': 'Meter status'},
        ),
    ]
    
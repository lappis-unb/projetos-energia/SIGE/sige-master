from django.urls import include, path
from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter

from apps.devices.views import (
    GlobalStatusViewSet,
    MeterModelViewSet,
    MeterStatusViewSet,
    MeterViewSet,
)

app_name = "meters"

router = DefaultRouter()
router.register(r"meters/status", GlobalStatusViewSet, basename="meter-status")
router.register(r"meter-models", MeterModelViewSet, basename="model")
router.register(r"meters", MeterViewSet, basename="meter")

meter_status_router = NestedDefaultRouter(router, r"meters", lookup="meter")
meter_status_router.register(r"status", MeterStatusViewSet, basename="meter-status")

urlpatterns = [
    path("", include(router.urls)),
    path("", include(meter_status_router.urls)),
]

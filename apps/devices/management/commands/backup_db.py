import logging
import subprocess

from django.conf import settings
from django.core.management.base import BaseCommand, CommandParser
from django.utils import timezone

from apps.utils.helpers import log_execution_time

logger = logging.getLogger("tasks") 


class Command(BaseCommand):
    help = "Backup the database."

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument("--max_backups", type=int, default=7)

    @log_execution_time(logger, level=logging.INFO)
    def handle(self, *args, **options) -> None:
        logger.info("Backup database - Starting...")
        
        db_config = settings.DATABASES["default"]
  
        max_backups = options["max_backups"]
        backup_path = settings.BASE_DIR / "backups"
        timestamp = timezone.localtime().strftime("%Y%m%d_%H%M%S")
        backup_file = backup_path / f"bkp_{ db_config['NAME']}_{timestamp}.dump"

        backup_path.mkdir(parents=True, exist_ok=True)
        logger.info(f"Backup path: {backup_file}")
        
        self.backup_db(db_config, backup_file)
        self.clean_backups(backup_path, max_backups)

    def backup_db(self, db_config, backup_file) -> None:
        logger.info("Running backup command...")
        
        base_command = [
            "pg_dump",
            "-h", db_config['HOST'],
            "-p", db_config['PORT'],
            "-U", db_config['USER'],
            "-d", db_config['NAME'],
            "-b",
            "-v",
            "--no-password", 
        ]
        
        gzip_command = [*base_command, "|", "gzip", ">", f"{backup_file}.gz"]
        env = {"PGPASSWORD": db_config["PASSWORD"]}
        
        try:
            result = subprocess.run(
                " ".join(gzip_command),
                check=True,
                capture_output=True,
                text=True,
                shell=True,
                env=env
            )
            logger.info(f"Successfully created backup: {backup_file}.gz")
            logger.debug(f"Command output: {result.stdout}")
        except subprocess.CalledProcessError as e:
            logger.error(f"pg_dump command failed: {e}")
            logger.error(f"Command output: {e.stdout}")
            logger.error(f"Command error: {e.stderr}")

    def clean_backups(self, backup_path, max_backups) -> None:
        logger.info("Verifying number of backups...")
        backups = sorted(backup_path.glob("*.dump.gz"), reverse=True)
        for backup in backups[max_backups:]:
            logger.info(f"Removing old backup: {backup}")
            backup.unlink()

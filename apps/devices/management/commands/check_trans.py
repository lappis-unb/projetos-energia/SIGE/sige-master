import logging
import multiprocessing

from concurrent.futures import ThreadPoolExecutor, as_completed

from django.core.management.base import BaseCommand, CommandParser
from pymodbus.client.tcp import ModbusTcpClient
from pymodbus.exceptions import ConnectionException

from apps.devices.models import Meter, Status
from apps.utils.helpers import log_execution_time

logger = logging.getLogger("tasks")


class Command(BaseCommand):
    help = "Test connection all meters."

    def add_arguments(self, parser: CommandParser) -> None:
        parser.add_argument("--max_workers", type=int, default=8)

    @log_execution_time(logger, level=logging.INFO)
    def handle(self, *args, **options) -> None:
        max_workers = options["max_workers"]
        meters = Meter.objects.broken_and_non_status()
        self.log_start(meters, max_workers)

        if meters.exists():
            self.process_broken_meters(meters, options["max_workers"])
        else:
            logger.info("Halted. No broken meters found.")

    def process_broken_meters(self, meters, max_workers):
        with ThreadPoolExecutor(max_workers=max_workers) as executor:
            future_list = []
            for meter in meters:
                future = executor.submit(self.check_meter, meter)
                future_list.append(future)

            for future in as_completed(future_list):
                try:
                    future.result()
                except Exception as e:
                    logger.error(f"Error processing meter: {e}")

    def check_meter(self, meter):
        client = ModbusTcpClient(meter.ip_address, meter.port)
        try:
            if client.connect():
                logger.info(f"Connection SUCCESS to meter at: {meter.ip_address}:{meter.port}")
                self.activate_meter(meter)
            else:
                logger.error(f"Connection FAILED to meter at: {meter.ip_address}:{meter.port}")
                self.deactivate_meter(meter)

        except ConnectionException as e:
            logger.error(f"Connection Error to meter at: {meter.ip_address}:{meter.port} - {str(e)}")

        finally:
            client.close()

    def activate_meter(self, meter):
        logger.info(f"Activated meter: {meter.ip_address}:{meter.port}.")
        meter.set_status(Status.ACTIVE, "Connection test successful.")

    def deactivate_meter(self, meter):
        logger.info(f"Deactivated meter: {meter.ip_address}:{meter.port}.")
        meter.set_status(Status.BROKEN, "Connection test failed.")

    def log_start(self, meters, max_workers):
        logger.info("   Check meters - Starting...")
        logger.debug(f"   MultiThread - Cores: {multiprocessing.cpu_count()}")
        logger.debug(f"   MultiThread - Max workers: {max_workers}")
        logger.debug("-" * 85)

        if meters.exists():
            logger.info(f"Broken Meters: {meters.count()}")
        else:
            logger.warning("No broken meters found.")

import logging

from django.db.models import Prefetch
from rest_framework import exceptions, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.common.pagination import BaseLimitOffsetPagination
from apps.common.serializers import DateRangeParamsValidator
from apps.devices.filters import MeterFilterSet, StatusHistoryFilterSet, StatusSummaryFilterSet
from apps.devices.models import Meter, MeterModel, StatusHistory
from apps.devices.serializers import (
    MeterCreateSerializer,
    MeterDetailSerializer,
    MeterListSerializer,
    MeterModelSerializer,
    MeterParamsValidator,
    MeterStatusDetailSerializer,
    StatusHistoryParamsValidator,
)
from apps.devices.services import MeterStatusAnalytics, MeterStatusReports

logger = logging.getLogger("apps.devices.views")


class MeterStatusPagination(BaseLimitOffsetPagination):
    default_limit = 20
    max_limit = 100


class MeterModelViewSet(viewsets.ModelViewSet):
    queryset = MeterModel.objects.all()
    serializer_class = MeterModelSerializer


class MeterViewSet(viewsets.ModelViewSet):
    queryset = Meter.objects.all()
    filterset_class = MeterFilterSet
    query_params_class = MeterParamsValidator

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.select_related("located", "located__parent", "model", "geo_location")

        return queryset.prefetch_related(
            Prefetch(
                "status_history",
                queryset=StatusHistory.objects.filter(end_time__isnull=True),
                to_attr="_current_status",
            )
        )

    def get_serializer_class(self):
        if self.action == "create":
            return MeterCreateSerializer
        if self.action == "retrieve":
            return MeterDetailSerializer
        return MeterListSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def filter_queryset(self, queryset):
        self.validated_params = self._validate_params(self.request.query_params)
        filterset = self.filterset_class(data=self.validated_params, queryset=queryset)
        return filterset.qs

    def _validate_params(self, query_params, raise_exception=True):
        params_serializer = self.query_params_class(data=query_params)
        params_serializer.is_valid(raise_exception=raise_exception)
        return params_serializer.validated_data


class BaseStatusViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = StatusHistory.objects.all()
    pagination_class = MeterStatusPagination
    filterset_class = StatusHistoryFilterSet
    serializer_class = MeterStatusDetailSerializer
    query_params_class = StatusHistoryParamsValidator

    parent_lookup = "meter_pk"

    def get_queryset(self):
        queryset = super().get_queryset().select_related("meter", "meter__model")

        if self.parent_lookup not in self.kwargs:
            return queryset

        try:
            meter_id = int(self.kwargs[self.parent_lookup])
            filtered_queryset = queryset.filter(meter_id=meter_id)
            self._validate_set_lookup(filtered_queryset, meter_id)
            return filtered_queryset
        except ValueError:
            raise exceptions.NotFound("Invalid URL: meter ID must be a positive number")

    def get_filterset_class(self):
        if self.action == "list":
            return StatusHistoryFilterSet
        if self.action == "summary":
            return StatusSummaryFilterSet
        return self.filterset_class

    def get_query_params_class(self):
        if self.action == "list":
            return StatusHistoryParamsValidator
        if self.action == "summary":
            return DateRangeParamsValidator
        return self.query_params_class

    def get_meter(self):
        return getattr(self, "_meter", None)

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context["meter_pk"] = self.kwargs.get("meter_pk")
        return context

    def filter_queryset(self, queryset):
        self.validated_params = self._validate_params(self.request.query_params)
        filterset_class = self.get_filterset_class()
        filterset = filterset_class(data=self.validated_params, queryset=queryset)
        return filterset.qs

    def _validate_params(self, query_params, raise_exception=True):
        validade_params_class = self.get_query_params_class()
        params_serializer = validade_params_class(data=query_params)
        params_serializer.is_valid(raise_exception=raise_exception)
        return params_serializer.validated_data

    def _validate_set_lookup(self, queryset, meter_id):
        try:
            self._meter = queryset.first().meter
        except AttributeError:
            if not Meter.objects.filter(id=meter_id).exists():
                raise exceptions.NotFound("Meter not found")
            raise exceptions.NotFound("No status found for this meter.")


class MeterStatusViewSet(BaseStatusViewSet):
    @action(methods=["get"], detail=False, url_path="summary")
    def summary(self, request, meter_pk=None):
        status_history_qs = self.filter_queryset(self.get_queryset())
        meter = self.get_meter()

        valid_params = self._validate_params(request.query_params)
        analytics = MeterStatusAnalytics(status_history_qs)
        report_service = MeterStatusReports(analytics, valid_params)

        response_data = report_service.get_meter_report(meter)
        return Response(response_data, status=status.HTTP_200_OK)


class GlobalStatusViewSet(BaseStatusViewSet):
    def list(self, request, *args, **kwargs):
        status_qs = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(status_qs)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        return Response("Pagination error", status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @action(detail=False, methods=["get"])
    def summary(self, request):
        status_history_qs = self.filter_queryset(self.get_queryset())
        analytics = MeterStatusAnalytics(status_history_qs)
        report_service = MeterStatusReports(analytics, self.validated_params)

        response_data = report_service.get_general_report()
        return Response(response_data, status=status.HTTP_200_OK)

from django.apps import AppConfig


class DevicesConfigConfig(AppConfig):
    name = "apps.devices"
    verbose_name = "Meters module"

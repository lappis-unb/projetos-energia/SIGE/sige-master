import math

from base64 import urlsafe_b64decode, urlsafe_b64encode
from collections import namedtuple
from datetime import datetime

from rest_framework.exceptions import NotFound
from rest_framework.pagination import CursorPagination, LimitOffsetPagination, PageNumberPagination
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param

DEFAULT_PAGE = 1


class EventPagination(PageNumberPagination):
    page_size_query_param = "page_size"

    def get_paginated_response(self, data):
        return Response(
            {
                "total": self.page.paginator.count,
                "total_pages": math.ceil(self.page.paginator.count / self.page_size),
                "current_page": int(self.request.GET.get("page", DEFAULT_PAGE)),
                "per_page": int(self.request.GET.get("page_size", self.page_size)),
                "links": {
                    "first": self.get_first_link(),
                    "prev": self.get_previous_link(),
                    "next": self.get_next_link(),
                    "last": self.get_last_link(),
                },
                "data": data,
            }
        )


class BaseLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 20
    max_limit = 500
    limit_query_param = "page_size"
    offset_query_param = "offset"

    def get_paginated_response(self, data):
        return Response(
            {
                "total": self.count,
                "total_pages": math.ceil(self.count / self.limit) if self.count > 0 else 0,
                "page_size": self.get_limit(self.request),
                "offset": self.get_offset(self.request),
                "links": {
                    "first": self.get_first_link(),
                    "prev": self.get_previous_link(),
                    "next": self.get_next_link(),
                    "last": self.get_last_link(),
                },
                "data": data,
            }
        )

    def get_first_link(self):
        if self.offset <= 0:
            return None

        url = self.request.build_absolute_uri()
        url = replace_query_param(url, self.limit_query_param, self.limit)
        return replace_query_param(url, self.offset_query_param, 0)

    def get_last_link(self):
        if self.offset + self.limit >= self.count:
            return None

        url = self.request.build_absolute_uri()
        url = replace_query_param(url, self.limit_query_param, self.limit)
        last_offset = (math.ceil(self.count / self.limit) - 1) * self.limit
        return replace_query_param(url, self.offset_query_param, last_offset)


Cursor = namedtuple("Cursor", ["offset", "reverse", "position"])


class BaseCursorPagination(CursorPagination):
    cursor_query_param = "cursor"
    page_size = 20
    max_page_size = 500
    page_size_query_param = "size"
    ordering = "-created_at"
    offset_cutoff = 1000

    def encode_cursor(self, cursor):
        if cursor.position is None:
            return None
        try:
            return self._encode_cursor(cursor)
        except (ValueError, TypeError):
            return None

    def decode_cursor(self, request):
        encoded = request.query_params.get(self.cursor_query_param)
        if not encoded:
            return None
        try:
            return self._decode_cursor(encoded)
        except (TypeError, ValueError):
            raise NotFound("Cursor inválido.")

    def _encode_cursor(self, cursor):
        if isinstance(cursor.position, str):
            dt = datetime.fromisoformat(cursor.position.replace("Z", "+00:00"))
            ts = int(dt.timestamp())
        else:
            ts = int(cursor.position)

        parts = [str(ts)]
        if cursor.reverse:
            parts.append("r")
        if cursor.offset:
            parts.append(f"o{cursor.offset}")

        token = "-".join(parts)
        encoded = urlsafe_b64encode(token.encode()).rstrip(b"=").decode()
        return replace_query_param(self.base_url, self.cursor_query_param, encoded)

    def _decode_cursor(self, encoded):
        padding = 4 - (len(encoded) % 4)
        if padding != 4:
            encoded += "=" * padding

        token = urlsafe_b64decode(encoded.encode()).decode()
        parts = token.split("-")

        ts = int(parts[0])
        reverse = "r" in parts[1:] if len(parts) > 1 else False

        offset = next((int(part[1:]) for part in parts[1:] if part.startswith("o")), 0)
        dt = datetime.fromtimestamp(ts).isoformat()
        return Cursor(offset=offset, reverse=reverse, position=dt)

    def get_paginated_response(self, data):
        return Response(
            {
                "page_size": self.get_page_size(self.request),
                "links": {
                    "next": self.get_next_link(),
                    "prev": self.get_previous_link(),
                },
                "data": data,
            }
        )

from django.conf import settings
from django.utils.translation import gettext_lazy as _


def error_msgs(field_type=""):
    MESSAGES = {
        "required": _("Missing required parameter."),
        "invalid_date": _("Date must be in ISO 8601 format (YYYY-MM-DDTHH:MM:SS)."),
    }

    response = {
        "required": MESSAGES["required"],
        "invalid": MESSAGES.get(f"invalid_{field_type}"),
    }
    response = {key: value for key, value in response.items() if value is not None}

    return response


def field_params(field_type):
    FIELD_INFO = {
        "fields": {
            "help_text": "Comma-separated list of fields the model to include in the response.",
        },
        "period": {
            "help_text": "Period to filter the data. Use 'number + unit' like '30D' for days.",
        },
        "date": {
            "help_text": "Must be in ISO 8601 format (YYYY-MM-DDTHH:MM:SS).",
        },
        "lttb": {
            "help_text": "Enable or disable the LTTB filter.",
            "default": True,
        },
        "threshold": {
            "help_text": "Threshold value to filter LTTB.",
            "default": settings.LTTB_THRESHOLD_POINTS,
        },
        "freq": {
            "help_text": "Frequency value to resample the data, like '1H' for hourly.",
        },
        "inc_desc": {
            "help_text": "Include entities descendants in the response.",
            "default": True,
        },
        "entity": {
            "help_text": "Entity ID to filter the data.",
        },
        "meter": {
            "help_text": "meter ID to filter measurements.",
        },
        "agg": {
            "help_text": "Aggregation function to resample the data.",
        },
        "depth": {
            "help_text": "Depth level of the entity hierarchy.",
            "default": 0,
        },
        "only_day": {
            "help_text": "Filter only data that occurred during the day period (solar time).",
            "default": False,
        },
        "th_percent": {
            "help_text": "Threshold value to filter the power factor.",
            "default": 92,
        },
        "detail_profile": {
            "help_text": "Return the detailed profile average per quarter-hour of the period.",
            "default": False,
        },
        "peak_hours": {
            "help_text": "Filter only data that occurred during the peak hours (18:00 - 20:59 UTC-3).",
            "default": False,
        },
        "off_peak_hours": {
            "help_text": "Filter data that occurred outside the peak hours (21:00 - 23:59 UTC-3).",
            "default": False,
        },
        "detail_report": {
            "help_text": "Return the detailed report with the aggregation per meter.",
            "default": False,
        },
    }

    msgs = error_msgs(field_type)
    info = FIELD_INFO.get(field_type, {})

    field_details = {
        "error_messages": msgs,
        "help_text": info.get("help_text", f"The {field_type} in a valid format."),
    }

    default_value = info.get("default", "")
    if default_value:
        field_details["default"] = default_value

    return field_details

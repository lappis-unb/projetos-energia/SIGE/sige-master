import logging

import pandas as pd

from django.db import models
from django.utils import timezone
from rest_framework import serializers
from rest_framework.serializers import Serializer, ValidationError

from apps.common.utils import field_params

logger = logging.getLogger("apps.common.serializers")


class TimeUnit(models.TextChoices):
    SECONDS = "s", "Seconds"
    MINUTES = "m", "Minutes"
    HOURS = "h", "Hours"
    DAYS = "d", "Days"


class AggregationType(models.TextChoices):
    SUM = "sum", "Sum"
    MEAN = "mean", "Mean"
    MAX = "max", "Max"
    MIN = "min", "Min"


class BaseParamsValidator(Serializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._setup_allowed_params()
        self._setup_required_params()

    def _setup_allowed_params(self):
        allowed_params = getattr(self.Meta, "params", [])
        for field_name in list(self.fields):
            if field_name not in allowed_params:
                self.fields.pop(field_name)

    def _setup_required_params(self):
        required_params = getattr(self.Meta, "required_params", [])
        for param in required_params:
            self.fields[param].required = True

    class Meta:
        params = []
        required_params = []
        max_period_days = None


class DateRangeParamsValidator(BaseParamsValidator):
    start_date = serializers.DateTimeField(required=False, **field_params("date"))
    end_date = serializers.DateTimeField(required=False, **field_params("date"))
    last = serializers.CharField(required=False, **field_params("last"))

    class Meta(BaseParamsValidator.Meta):
        params = ["start_date", "end_date", "last"]

    def validate_last(self, value):
        try:
            last = pd.to_timedelta(value)
            if last.total_seconds() <= 0:
                raise ValidationError(f"Invalid value: {value}. Must be greater than 0")
            return last
        except ValueError:
            raise ValidationError(
                f"Invalid format: '{value}'. Use format: '<number><unit>' (e.g., '60s', '15m', '12h', '7d')."
            )

    def validate(self, attrs):
        start_date = attrs.get("start_date")
        end_date = attrs.get("end_date", timezone.now())
        last = attrs.get("last")

        if start_date and last:
            raise ValidationError("Cannot specify both 'start_date' and 'last'")

        if last and attrs.get("end_date"):
            raise ValidationError("Cannot specify both 'end_date' and 'last'")

        if start_date:
            self._validate_start_date(start_date, end_date)
            self._validate_period_length(start_date, end_date)
            return attrs

        if last:
            start_date = timezone.now() - last
            self._validate_period_length(start_date, timezone.now())
            attrs.update({"start_date": start_date})
            attrs.pop("last")

        return attrs

    def _validate_start_date(self, start_date, end_date):
        if start_date > timezone.now():
            raise ValidationError({"start_date": "Start date cannot be in the future"})
        if start_date > end_date:
            raise ValidationError({"start_date": "Start date cannot be after end date"})

    def _validate_period_length(self, start_date, end_date):
        max_period_days = getattr(self.Meta, "max_period_days", None)
        if max_period_days is not None:
            period_days = (end_date - start_date).days
            if period_days > max_period_days:
                raise ValidationError(f"The selected period exceeds the {max_period_days} days limit")
